# https://twiki.cern.ch/twiki/bin/view/AtlasProtected/GoodRunListsForAnalysisRun2
# ilumicalc is averaged mu; actualMu is actual mu
_pathlcalc = '/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/GoodRunsLists/'
if mc16campaign == 'mc16a':
  alg_prw.prw.LumiCalcFiles = [
                                   _pathlcalc+'data16_13TeV/20180129/PHYS_StandardGRL_All_Good_25ns_297730-311481_OflLumi-13TeV-009.root', \
                                   _pathlcalc+'data15_13TeV/20170619/PHYS_StandardGRL_All_Good_25ns_276262-284484_OflLumi-13TeV-008.root', \
                                 ]
elif mc16campaign == 'mc16d':
  alg_prw.prw.LumiCalcFiles = [
                                   _pathlcalc+'data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.lumicalc.OflLumi-13TeV-010.root', \
                                 ]
elif mc16campaign == 'mc16e':
  alg_prw.prw.LumiCalcFiles = [ _pathlcalc+'data18_13TeV/20190318/ilumicalc_histograms_None_348885-364292_OflLumi-13TeV-010.root', \
                                 ]
else:
  print( 'mc16campaign = {0} is not defined'.format(mc16campaign) )
#  _pathlcalc+'data18_13TeV/20190318/physics_25ns_Triggerno17e33prim.actualMu.OflLumi-13TeV-010.root', \
#  _pathlcalc+'data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.actualMu.OflLumi-13TeV-010.root', \

# process dependent
# TODO combining MC16a+d+e or do separate trees and merge later
# VERY IMPORTANT: You must not use default config files for combining MC16a+d+e!
# TODO upgrade to actualMu that requires special prw files
# https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/ExtendedPileupReweighting#Generating_the_actual_pileup_fil
_pathprw = '/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/dev/PileupReweighting/mc16_13TeV/'
alg_prw.prw.ConfigFiles = [ _pathprw+'pileup_'+mc16campaign+'_dsid361106_FS.root', \
                            _pathprw+'pileup_'+mc16campaign+'_dsid361107_FS.root', \
                            _pathprw+'pileup_'+mc16campaign+'_dsid364100_FS.root', \
                            _pathprw+'pileup_'+mc16campaign+'_dsid364101_FS.root', \
                            _pathprw+'pileup_'+mc16campaign+'_dsid364102_FS.root', \
                            _pathprw+'pileup_'+mc16campaign+'_dsid364103_FS.root', \
                            _pathprw+'pileup_'+mc16campaign+'_dsid364104_FS.root', \
                            _pathprw+'pileup_'+mc16campaign+'_dsid364105_FS.root', \
                            _pathprw+'pileup_'+mc16campaign+'_dsid364106_FS.root', \
                            _pathprw+'pileup_'+mc16campaign+'_dsid364107_FS.root', \
                            _pathprw+'pileup_'+mc16campaign+'_dsid364108_FS.root', \
                            _pathprw+'pileup_'+mc16campaign+'_dsid364109_FS.root', \
                            _pathprw+'pileup_'+mc16campaign+'_dsid364110_FS.root', \
                            _pathprw+'pileup_'+mc16campaign+'_dsid364111_FS.root', \
                            _pathprw+'pileup_'+mc16campaign+'_dsid364112_FS.root', \
                            _pathprw+'pileup_'+mc16campaign+'_dsid364113_FS.root', \
                            _pathprw+'pileup_'+mc16campaign+'_dsid364114_FS.root', \
                            _pathprw+'pileup_'+mc16campaign+'_dsid364115_FS.root', \
                            _pathprw+'pileup_'+mc16campaign+'_dsid364116_FS.root', \
                            _pathprw+'pileup_'+mc16campaign+'_dsid364117_FS.root', \
                            _pathprw+'pileup_'+mc16campaign+'_dsid364118_FS.root', \
                            _pathprw+'pileup_'+mc16campaign+'_dsid364119_FS.root', \
                            _pathprw+'pileup_'+mc16campaign+'_dsid364120_FS.root', \
                            _pathprw+'pileup_'+mc16campaign+'_dsid364121_FS.root', \
                            _pathprw+'pileup_'+mc16campaign+'_dsid364122_FS.root', \
                            _pathprw+'pileup_'+mc16campaign+'_dsid364123_FS.root', \
                            _pathprw+'pileup_'+mc16campaign+'_dsid364124_FS.root', \
                            _pathprw+'pileup_'+mc16campaign+'_dsid364125_FS.root', \
                            _pathprw+'pileup_'+mc16campaign+'_dsid364126_FS.root', \
                            _pathprw+'pileup_'+mc16campaign+'_dsid364127_FS.root', \
                            _pathprw+'pileup_'+mc16campaign+'_dsid426335_FS.root', \
                            _pathprw+'pileup_'+mc16campaign+'_dsid426336_FS.root', \
                          ]

