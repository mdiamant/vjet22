#
# https://twiki.cern.ch/twiki/bin/view/Atlas/LowestUnprescaled

### ### ###
# strategy of being historical consistent and not changing offline lepton pT thresholds (20GeV)
### ### ###
# if avoid accidentially pre-scaled triggers, the threshold in triggers would be higher than 20GeV
# given the fact only very small fraction of evets were accidentically pre-scaled
# and the fact that no event counting in data or MC will be used directly, but only the response
# go with historical triggers
# https://cds.cern.ch/record/2669655/files/ATL-COM-PHYS-2019-281.pdf
alg_selevt.triglist201516 = ['HLT_2mu10']
alg_selevt.triglist2017 = ['HLT_2mu14']
alg_selevt.triglist2018 = ['HLT_2mu14']

