# muon container
alg_selmuon.in_collection = 'Muons'

# muon calibration
# https://twiki.cern.ch/twiki/bin/view/AtlasProtected/MCPAnalysisWinterMC16#Momentum_corrections
alg_selmuon.calibration.Release = 'Recs2019_05_30'
if '2015-2016' == year:
  alg_selmuon.calibration.Year = 'Data16' # Data17, Data18, Data16, Data15
elif '2017' == year:
  alg_selmuon.calibration.Year = 'Data17'
elif '2018' == year:
  alg_selmuon.calibration.Year = 'Data18'
alg_selmuon.calibration.Algo = 'muons'
alg_selmuon.calibration.SmearingType = 'q_pT'
alg_selmuon.calibration.StatComb = False
alg_selmuon.calibration.SagittaCorr = False
if '2015-2016' == year:
  alg_selmuon.calibration.SagittaRelease = 'sagittaBiasDataAll_03_02_19_Data16'
elif '2017' == year:
  alg_selmuon.calibration.SagittaRelease = 'sagittaBiasDataAll_03_02_19_Data17'
elif '2018' == year:
  alg_selmuon.calibration.SagittaRelease = 'sagittaBiasDataAll_03_02_19_Data18'
alg_selmuon.calibration.doSagittaMCDistortion = False
alg_selmuon.calibration.SagittaCorrPhaseSpace = True
alg_selmuon.calibration.noEigenDecor = False

# muon selection
# https://twiki.cern.ch/twiki/bin/view/Atlas/MuonSelectionToolR21
alg_selmuon.pTmin = 20 # pT > 20 GeV
alg_selmuon.selector.MaxEta = 2.4 # |eta| < 2.4
alg_selmuon.selector.MuQuality = 3 # ID: Tight, Medium, Loose, VeryLoose, HighPt, LowPt -> 0, 1, 2, 3, 4 and 5
alg_selmuon.isolation.MuonWP = 'FCLoose'

