# jet container
alg_seljetR04.in_collection = 'AntiKt4EMPFlowJets'

# jet calibration for insitu calibration use
# https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/ApplyJetCalibrationR21#Jet_expert_level_calibrations
alg_seljetR04.calibration.JetCollection = 'AntiKt4EMPFlow'
if '201516' == year:
  alg_seljetR04.calibration.ConfigFile = 'JES_MC16Recommendation_PFlow_EtaInterCalibrationOnly_20Dec2017.config'
elif '2017' == year or '2018' == year:
  # !!!!!!!!!!!!!!!!!! wait for updates for 2018 config file !!!!!!!!!!!!!!!!!!!!!!!!
  alg_seljetR04.calibration.ConfigFile = 'JES_2017data_PFlow_EtaInterCalibrationOnly_08Feb2018.config'
else:
  print('{0} is not available for jet calibration. EXIT'.format(year))
  exit(1)
alg_seljetR04.calibration.CalibSequence = 'JetArea_Residual_EtaJES_GSC'
alg_seljetR04.calibration.CalibArea = '00-04-81'
alg_seljetR04.calibration.IsData = isdata

# jvt
# https://twiki.cern.ch/twiki/bin/view/AtlasProtected/PileupJetRecommendations
# apply_jvt.WorkingPoint and apply_jvt.MaxPtForJvt are not in use
# as the jvt cut is made by hand in the code
alg_seljetR04.apply_jvt.WorkingPoint = 'Tight' # default: EMTopo=Medium EMFlow=Tight
alg_seljetR04.apply_jvt.SFFile = 'JetJvtEfficiency/Moriond2018/JvtSFFile_EMPFlow.root' # 'JetJvtEfficiency/Moriond2018/JvtSFFile_EMTopoJets.root'
alg_seljetR04.apply_jvt.MaxPtForJvt = 60000 # 120 GeV is optional

alg_seljetR04.pTmin = 10
alg_seljetR04.etamax = 4.5

