#!/usr/bin/env python

# functions
def include(filename):
  _filename = os.path.expandvars('$UserAnalysis_DIR/bin/{0}'.format(filename))
  if os.path.exists(_filename): 
    execfile(_filename)
  else:
    print('File {0} does NOT exist. EXIT'.format(_filename))
    exit(1)

# Read the submission directory as a command line argument. You can
# extend the list of arguments with your private ones later on.
import optparse
parser = optparse.OptionParser()
parser.add_option( '-s', '--submission-dir', dest = 'submission_dir',
                   action = 'store', type = 'string', default = 'submitDir',
                   help = 'Submission directory for EventLoop' )
( options, args ) = parser.parse_args()

# Set up (Py)ROOT.
import ROOT
ROOT.xAOD.Init().ignore()

# Set up the sample handler object. See comments from the C++ macro
# for the details about these lines.
import os
sh = ROOT.SH.SampleHandler()
sh.setMetaString( 'vjet_tree', 'CollectionTree' )
#inputFilePath = '/eos/user/x/xiaohu/Jet/dataset/mc16_13TeV.361107.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zmumu.deriv.DAOD_JETM3.e3601_s3126_r9364_r9315_p4060/'
#inputFilePath = '/eos/user/x/xiaohu/Jet/dataset/mc16_13TeV.361106.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zee.deriv.DAOD_JETM3.e3601_s3126_r9364_r9315_p4060/'
inputFilePath = '/eos/user/x/xiaohu/Jet/dataset/mc16_13TeV.423099.Pythia8EvtGen_A14NNPDF23LO_gammajet_DP8_17.deriv.DAOD_JETM4.e4453_s3126_r10201_r10210_p3401/'
#inputFilePath = '/eos/user/x/xiaohu/Jet/dataset/data17_13TeV.periodB.physics_Main.PhysCont.DAOD_JETM3.grp18_v01_p3841/'
ROOT.SH.ScanDir().filePattern( 'DAOD*.pool.root.1' ).scan( sh, inputFilePath )
sh.printContent()

# Create an EventLoop job.
job = ROOT.EL.Job()
job.sampleHandler( sh )
job.options().setDouble( ROOT.EL.Job.optMaxEvents, 10 )

# set global variables
year = '2017' # '201516' '2017' '2018'
isdata = True

# Create the algorithm's configuration.
from AnaAlgorithm.AnaAlgorithmConfig import AnaAlgorithmConfig
alg_selgam = AnaAlgorithmConfig( 'VjTM::Gamma/selgam' )
#alg_selelec = AnaAlgorithmConfig( 'VjTM::Electron/selelec' )
#alg_selmuon = AnaAlgorithmConfig( 'VjTM::Muon/selmuon' )
alg_seljet = AnaAlgorithmConfig( 'VjTM::Jet/seljet' )
alg_calcmet = AnaAlgorithmConfig('VjTM::MET/calcmet')
alg_selevt = AnaAlgorithmConfig( 'VjTM::EventSelection/selevt' )
alg_mktr = AnaAlgorithmConfig( 'VjTM::TreeMaker/mktr' )

#
from AnaAlgorithm.DualUseConfig import addPrivateTool
addPrivateTool( alg_seljet, 'calibration', 'JetCalibrationTool' )
include('vjet_jet_config.py')
alg_seljet.out_collection = 'jets'

#
addPrivateTool( alg_selgam, 'calibration', 'CP::EgammaCalibrationAndSmearingTool' )
addPrivateTool( alg_selgam, 'selector', 'AsgPhotonIsEMSelector' )
addPrivateTool( alg_selgam, 'isolation', 'CP::IsolationSelectionTool' )
alg_selgam.in_collection = 'Photons'
include('vjet_gamma_config.py')
alg_selgam.out_collection = 'gammas'

#
#addPrivateTool( alg_selelec, 'calibration', 'CP::EgammaCalibrationAndSmearingTool' )
#addPrivateTool( alg_selelec, 'selector', 'AsgElectronLikelihoodTool' )
#addPrivateTool( alg_selelec, 'isolation', 'CP::IsolationSelectionTool' )
#alg_selelec.in_collection = 'Electrons'
#include('vjet_electron_config.py')
#alg_selelec.out_collection = 'electrons'

#
#addPrivateTool( alg_selmuon, 'calibration', 'CP::MuonCalibrationAndSmearingTool' )
#addPrivateTool( alg_selmuon, 'selector', 'CP::MuonSelectionTool' )
#addPrivateTool( alg_selmuon, 'isolation', 'CP::IsolationSelectionTool' )
#include('vjet_muon_config.py')
#alg_selmuon.out_collection = 'muons'

#
addPrivateTool( alg_calcmet, 'calibration', 'met::METMaker' )
alg_calcmet.in_collection = 'NULL' # auto made with jet type below
alg_calcmet.out_collection = 'met'
alg_calcmet.process = 'GammaJet' # ZeeJet ZmmJet GammaJet
alg_calcmet.jettype = alg_seljet.calibration.JetCollection
#alg_calcmet.in_electron = alg_selelec.out_collection
#alg_calcmet.in_muon = alg_selmuon.out_collection
alg_calcmet.in_gamma = alg_selgam.out_collection
alg_calcmet.in_jet = alg_seljet.out_collection
#alg_calcmet.calibration...

#
alg_selevt.jet_collection = alg_seljet.out_collection
alg_selevt.OutputLevel = ROOT.MSG.DEBUG

#
job.outputAdd (ROOT.EL.OutputStream ('ANALYSIS'))
alg_mktr.in_jet = alg_seljet.out_collection

# Add our algorithm to the job
job.algsAdd( alg_selgam )
#job.algsAdd( alg_selelec )
#job.algsAdd( alg_selmuon )
job.algsAdd( alg_seljet )
job.algsAdd( alg_calcmet )
job.algsAdd( alg_selevt )
job.algsAdd( alg_mktr )

# Run the job using the direct driver.
driver = ROOT.EL.DirectDriver()
driver.submit( job, options.submission_dir )
