alg_selelec.in_collection = 'Electrons'

# electron calibration
# https://twiki.cern.ch/twiki/bin/view/AtlasProtected/ElectronPhotonFourMomentumCorrection
alg_selelec.calibration.ESModel = 'es2018_R21_v0'
alg_selelec.calibration.decorrelationModel = 'FULL_v1'
#alg_selelec.calibration.randomRunNumber = 123456 # need to apply pileup tool in advance to replace this !!!

# electron selection
# https://twiki.cern.ch/twiki/bin/view/AtlasProtected/EGammaIdentificationRun2#Electron_Identification
#alg_selelec.selector.WorkingPoint = 'LooseLHElectron'
alg_selelec.idWorkingPoint = 'ElectronsLHLoose' # trim off DFCommon 
alg_selelec.isolation.ElectronWP = 'FCLoose'
alg_selelec.pTmin = 20 # GeV
alg_selelec.etamax = 2.47 # EM crack is hard coded

# scale factors
# use the "map" method
# MapFilePath is hard coded in the tool
# https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/XAODElectronEfficiencyCorrectionTool
# now using trigger with isolation "...VHI" in which a small fraction of events are prescaled in 2017
# negligible level and effect to jet analysis
# set all trig id and iso
alg_selelec.effcorr_trig.TriggerKey = 'DI_E_2015_e12_lhloose_L1EM10VH_2016_e17_lhvloose_nod0_2017_2018_e17_lhvloose_nod0_L1EM15VHI'
alg_selelec.effcorr_trig.IdKey = 'LooseBLayer' # there is not Loose ...
alg_selelec.effcorr_trig.IsoKey = 'FCLoose'
# set only reco
alg_selelec.effcorr_reco.RecoKey = 'Reconstruction'
# set only id
alg_selelec.effcorr_id.IdKey = 'LooseBLayer'
# set both id and iso
alg_selelec.effcorr_iso.IdKey = 'LooseBLayer'
alg_selelec.effcorr_iso.IsoKey = 'FCLoose'
# importantly set the key for modified eventInfo in upstream algos
#alg_selelec.effcorr_trig.EventInfoCollectionName = 'mcEventInfo' # hard coded in Pileup.cxx
#alg_selelec.effcorr_reco.EventInfoCollectionName = 'mcEventInfo' # hard coded in Pileup.cxx
#alg_selelec.effcorr_id.EventInfoCollectionName = 'mcEventInfo' # hard coded in Pileup.cxx
#alg_selelec.effcorr_iso.EventInfoCollectionName = 'mcEventInfo' # hard coded in Pileup.cxx

alg_selelec.effcorr_trig.EventInfoCollectionName = 'EventInfo' # hard coded in Pileup.cxx
alg_selelec.effcorr_reco.EventInfoCollectionName = 'EventInfo' # hard coded in Pileup.cxx
alg_selelec.effcorr_id.EventInfoCollectionName = 'EventInfo' # hard coded in Pileup.cxx
alg_selelec.effcorr_iso.EventInfoCollectionName = 'EventInfo' # hard coded in Pileup.cxx
