# jet container
alg_seljetR10.in_collection = 'AntiKt10LCTopoTrimmedPtFrac5SmallR20Jets'

# jet calibration for insitu calibration use
# https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/ApplyJetCalibrationR21
alg_seljetR10.calibration.JetCollection = 'AntiKt10LCTopoTrimmedPtFrac5SmallR20'

# for the moment no year dependence
if isdata:
  alg_seljetR10.calibration.ConfigFile = 'JES_MC16recommendation_FatJet_Trimmed_JMS_comb_3April2019.config'
  alg_seljetR10.calibration.CalibSequence = 'EtaJES_JMS' # 'EtaJES_JMS_Insitu_InsituCombinedMass'
else:
  alg_seljetR10.calibration.ConfigFile = 'JES_MC16recommendation_FatJet_Trimmed_JMS_comb_17Oct2018.config'
  alg_seljetR10.calibration.CalibSequence = 'EtaJES_JMS'

alg_seljetR10.calibration.CalibArea = '00-04-82'
alg_seljetR10.calibration.IsData = isdata

# jet selection
alg_seljetR10.pTmin = 100 # GeV
alg_seljetR10.etamax = 0.8 # eta_deta ?
