#!/usr/bin/env python
import sys

# functions
def include(filename):
  _filename = os.path.expandvars('$UserAnalysis_DIR/bin/{0}'.format(filename))
  if os.path.exists(_filename): 
    exec(open(_filename).read())#otherwise it does not recognise execfile 
  else:
    print('File {0} does NOT exist. EXIT'.format(_filename))
    exit(1)

# Read the submission directory as a command line argument. You can
# extend the list of arguments with your private ones later on.
import optparse
parser = optparse.OptionParser()
parser.add_option( '-s', '--submission-dir', dest = 'submission_dir',
                   action = 'store', type = 'string', default = 'submitDir',
                   help = 'Submission directory for EventLoop' )
parser.add_option( '-y', '--year', dest = 'year',
                   action = 'store', type = 'string', default = '201516',
                   help = 'Data taking year: 201516 2017 2018' )
parser.add_option( '-m', '--mode', dest = 'mode',
                   action = 'store', type = 'string', default = 'local',
                   help = 'Run mode: local grid' )
parser.add_option( '-i', '--dinput', dest = 'dinput',
                   action = 'store', type = 'string', default = '/eos/user/m/mdiamant/vjet/',
                   help = 'Input: absolute local path or grid data container name' )
parser.add_option( '-t', '--dtype', dest = 'dtype',
                   action = 'store', type = 'string', default = 'mc',
                   help = 'Data type: data mc' )
parser.add_option( '-g', '--gridoutput', dest = 'gridoutput',
                   action = 'store', type = 'string', default = 'user.xiaohu.vjet.testv0',
                   help = 'Grid-only output container: user.xiaohu.vjet.testv0 etc.' )
parser.add_option( '-n', '--maxevent', dest = 'maxevent',
                   action = 'store', type = 'int', default = '-1',
                   help = 'Nb of max event to run: -1 means no limit' )
( options, args ) = parser.parse_args()

# set global variables (to the old interface: local variables)
year = options.year # '201516' '2017' '2018'
isdata = False
if options.dtype == 'data':
  isdata = True
else:
  isdata = False
isgrid = False
if options.mode == 'grid':
  isgrid = True
else:
  isgrid = False
dataset = options.dinput # either local path or grid container name
outdataset = options.gridoutput
maxevent = options.maxevent
#
mcdict = { '201516':'mc16a', '2017':'mc16d', '2018':'mc16e' }
mc16campaign = mcdict[year]
#
print('JO::year {0}'.format(year))
print('JO::isdata {0}'.format(isdata))
print('JO::isgrid {0}'.format(isgrid))
print('JO::dataset {0}'.format(dataset))
print('JO::outdataset {0}'.format(outdataset))
print('JO::maxevent {0}'.format(maxevent))
print('JO::mc {0}'.format(mc16campaign))
#

# Set up (Py)ROOT.
import ROOT
ROOT.xAOD.Init().ignore()

# Set up the sample handler object. See comments from the C++ macro
# for the details about these lines.
import os
sh = ROOT.SH.SampleHandler()
sh.setMetaString( 'vjet_tree', 'CollectionTree' )
#
if isgrid:
  ROOT.SH.scanRucio (sh, dataset)
else: # local
  inputFilePath = dataset
  ROOT.SH.ScanDir().filePattern( 'DAOD*.pool.root.1' ).scan( sh, inputFilePath )
sh.printContent()

# Create an EventLoop job.
job = ROOT.EL.Job()
job.sampleHandler( sh )
if isgrid:
  pass
else:
  if maxevent != -1:
    job.options().setDouble( ROOT.EL.Job.optMaxEvents, maxevent )

# Create the algorithm's configuration.
from AnaAlgorithm.AnaAlgorithmConfig import AnaAlgorithmConfig
if not isdata:
 alg_prw = AnaAlgorithmConfig( 'VjTM::Pileup/prw' )
alg_selelec = AnaAlgorithmConfig( 'VjTM::Electron/selelec' )
alg_seljetR04 = AnaAlgorithmConfig( 'VjTM::JetR04/seljetR04' )
alg_calcmet = AnaAlgorithmConfig('VjTM::MET/calcmet')
alg_selevt = AnaAlgorithmConfig( 'VjTM::EventSelection_ZjetR04/selevt' )

#
from AnaAlgorithm.DualUseConfig import addPrivateTool
if not isdata:
  addPrivateTool( alg_prw, 'prw', 'CP::PileupReweightingTool' )
  include('vjet_prw_config.py')

#
addPrivateTool( alg_selelec, 'calibration', 'CP::EgammaCalibrationAndSmearingTool' )
#addPrivateTool( alg_selelec, 'selector', 'AsgElectronLikelihoodTool' )
addPrivateTool( alg_selelec, 'effcorr_trig', 'AsgElectronEfficiencyCorrectionTool' )
addPrivateTool( alg_selelec, 'effcorr_reco', 'AsgElectronEfficiencyCorrectionTool' )
addPrivateTool( alg_selelec, 'effcorr_id', 'AsgElectronEfficiencyCorrectionTool' )
addPrivateTool( alg_selelec, 'effcorr_iso', 'AsgElectronEfficiencyCorrectionTool' )
addPrivateTool( alg_selelec, 'isolation', 'CP::IsolationSelectionTool' )
include('vjet_electron_forjetR04_config.py')
alg_selelec.out_collection = 'electrons'

#
addPrivateTool( alg_seljetR04, 'calibration', 'JetCalibrationTool' )
addPrivateTool( alg_seljetR04, 'calibration_jvt', 'JetVertexTaggerTool' )
addPrivateTool( alg_seljetR04, 'apply_jvt', 'CP::JetJvtEfficiency' )
include('vjet_jetR04_config.py')
alg_seljetR04.out_collection = 'jetsR04'

#
addPrivateTool( alg_calcmet, 'calibration', 'met::METMaker' )
include('vjet_met_forjetR04_config.py')
alg_calcmet.process = 'ZeeJet' # ZeeJet ZmmJet GammaJet
alg_calcmet.jettype = alg_seljetR04.calibration.JetCollection
alg_calcmet.in_electron = alg_selelec.out_collection
alg_calcmet.in_jet = alg_seljetR04.out_collection
alg_calcmet.out_collection = 'met'

#
alg_selevt.process = 'ZeeJet'
addPrivateTool( alg_selevt, 'grl', 'GoodRunsListSelectionTool' )
include('vjet_grl_config.py')
include('vjet_trigZee_config.py')
alg_selevt.isdata = isdata
alg_selevt.year = year
alg_selevt.elec_collection = alg_selelec.out_collection
alg_selevt.jetR04_collection = alg_seljetR04.out_collection
alg_selevt.met_collection = alg_calcmet.out_collection
alg_selevt.OutputLevel = ROOT.MSG.INFO # ROOT.MSG.DEBUG,INFO

#
job.outputAdd (ROOT.EL.OutputStream ('ANALYSIS'))

# Add our algorithm to the job
#if not isdata:
job.algsAdd( alg_prw )
job.algsAdd( alg_selelec )
job.algsAdd( alg_seljetR04 )
job.algsAdd( alg_calcmet )
job.algsAdd( alg_selevt )

# Run the job using the direct driver.
if isgrid:
  driver = ROOT.EL.PrunDriver()
  driver.options().setString("nc_outputSampleName", "{0}.%in:name[2]%.%in:name[6]%".format(outdataset))
  driver.submitOnly( job, options.submission_dir )
else: # local
  driver = ROOT.EL.DirectDriver()
  driver.submit( job, options.submission_dir )
