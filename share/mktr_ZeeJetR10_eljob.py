#!/usr/bin/env python

# functions
def include(filename):
  _filename = os.path.expandvars('$UserAnalysis_DIR/bin/{0}'.format(filename))
  if os.path.exists(_filename): 
    execfile(_filename)
  else:
    print('File {0} does NOT exist. EXIT'.format(_filename))
    exit(1)

# Read the submission directory as a command line argument. You can
# extend the list of arguments with your private ones later on.
import optparse
parser = optparse.OptionParser()
parser.add_option( '-s', '--submission-dir', dest = 'submission_dir',
                   action = 'store', type = 'string', default = 'submitDir',
                   help = 'Submission directory for EventLoop' )
( options, args ) = parser.parse_args()

# Set up (Py)ROOT.
import ROOT
ROOT.xAOD.Init().ignore()

# Set up the sample handler object. See comments from the C++ macro
# for the details about these lines.
import os
sh = ROOT.SH.SampleHandler()
sh.setMetaString( 'vjet_tree', 'CollectionTree' )
inputFilePath = '/eos/user/x/xiaohu/Jet/dataset/mc16_13TeV.364120.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV140_280_CVetoBVeto.deriv.DAOD_JETM3.e5299_s3126_r9364_p4049/'
#ROOT.SH.ScanDir().filePattern( 'DAOD*.pool.root.1' ).scan( sh, inputFilePath )

ROOT.SH.scanRucio (sh, "mc16_13TeV.364120.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV140_280_CVetoBVeto.deriv.DAOD_JETM3.e5299_s3126_r9364_p4049")

sh.printContent()

# Create an EventLoop job.
job = ROOT.EL.Job()
job.sampleHandler( sh )
#job.options().setDouble( ROOT.EL.Job.optMaxEvents, 10 )

# set global variables
year = '2015-2016' # '2015-2016' '2017' '2018'
isdata = False

# Create the algorithm's configuration.
from AnaAlgorithm.AnaAlgorithmConfig import AnaAlgorithmConfig
alg_selelec = AnaAlgorithmConfig( 'VjTM::Electron/selelec' )
alg_seljetR04 = AnaAlgorithmConfig( 'VjTM::JetR04/seljetR04' )
alg_seljetR10 = AnaAlgorithmConfig( 'VjTM::JetR10/seljetR10' )
alg_selevt = AnaAlgorithmConfig( 'VjTM::EventSelection_ZjetR10/selevt' )

#
from AnaAlgorithm.DualUseConfig import addPrivateTool
addPrivateTool( alg_selelec, 'calibration', 'CP::EgammaCalibrationAndSmearingTool' )
#addPrivateTool( alg_selelec, 'selector', 'AsgElectronLikelihoodTool' )
addPrivateTool( alg_selelec, 'isolation', 'CP::IsolationSelectionTool' )
alg_selelec.in_collection = 'Electrons'
include('vjet_electron_forjetR10_config.py')
alg_selelec.out_collection = 'electrons'

#
addPrivateTool( alg_seljetR04, 'calibration', 'JetCalibrationTool' )
addPrivateTool( alg_seljetR04, 'calibration_jvt', 'JetVertexTaggerTool' )
addPrivateTool( alg_seljetR04, 'apply_jvt', 'CP::JetJvtEfficiency' )
include('vjet_jetR04_config.py')
alg_seljetR04.out_collection = 'jetsR04'

#
from AnaAlgorithm.DualUseConfig import addPrivateTool
addPrivateTool( alg_seljetR10, 'calibration', 'JetCalibrationTool' )
include('vjet_jetR10_config.py')
alg_seljetR10.out_collection = 'jetsR10'

#
# R10 jets only apply DB, but not MPF, thus no MET needed

#
alg_selevt.process = 'ZeeJet'
addPrivateTool( alg_selevt, 'grl', 'GoodRunsListSelectionTool' )
include('vjet_grl_config.py')
alg_selevt.isdata = isdata
alg_selevt.elec_collection = alg_selelec.out_collection
alg_selevt.jetR04_collection = alg_seljetR04.out_collection
alg_selevt.jetR10_collection = alg_seljetR10.out_collection
alg_selevt.OutputLevel = ROOT.MSG.INFO # ROOT.MSG.DEBUG,INFO

#
job.outputAdd (ROOT.EL.OutputStream ('ANALYSIS'))

# Add our algorithm to the job
job.algsAdd( alg_selelec )
job.algsAdd( alg_seljetR04 )
job.algsAdd( alg_seljetR10 )
job.algsAdd( alg_selevt )

# Run the job using the direct driver.
#driver = ROOT.EL.DirectDriver()
#driver.submit( job, options.submission_dir )
driver = ROOT.EL.PrunDriver()
driver.options().setString("nc_outputSampleName", "user.xiaohu.vjet.test.%in:name[2]%.%in:name[6]%")
driver.submitOnly( job, options.submission_dir )
