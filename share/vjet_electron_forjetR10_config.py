alg_selelec.in_collection = 'Electrons'

# electron calibration
# https://twiki.cern.ch/twiki/bin/view/AtlasProtected/ElectronPhotonFourMomentumCorrection
alg_selelec.calibration.ESModel = 'es2018_R21_v0'
alg_selelec.calibration.decorrelationModel = 'FULL_v1'
alg_selelec.calibration.randomRunNumber = 123456 # need to apply pileup tool in advance to replace this !!!

# electron selection
# https://twiki.cern.ch/twiki/bin/view/AtlasProtected/EGammaIdentificationRun2#Electron_Identification
#alg_selelec.selector.WorkingPoint = 'MediumLHElectron'
alg_selelec.idWorkingPoint = 'ElectronsLHMedium' # trim off DFCommon
alg_selelec.isolation.ElectronWP = 'FCLoose'
alg_selelec.pTmin = 20 # GeV
alg_selelec.etamax = 2.47 # EM crack is hard coded
