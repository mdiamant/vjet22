#
_pathgrl = '/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/GoodRunsLists/'
alg_selevt.grl.GoodRunsListVec = [ _pathgrl+'data18_13TeV/20190318/physics_25ns_Triggerno17e33prim.xml', \
                                   _pathgrl+'data17_13TeV/20180619/physics_25ns_Triggerno17e33prim.xml', \
								   _pathgrl+'data16_13TeV/20180129/physics_25ns_21.0.19.xml', \
								   _pathgrl+'data15_13TeV/20170619/physics_25ns_21.0.19.xml', \
								 ]

# https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/GoodRunListsForAnalysisRun2
#58450.1 pb-1
#2018 pp collisions dataset (full set, latest luminosity tag)
#Tag pro22-04 (2018 data periods B-Q, full 2018, latest lumi calibration for 2018, OflLumi -13TeV-010) is copied in https://atlas-groupdata.web.cern.ch/atlas-groupdata/GoodRunsLists/data18_13TeV/20190318
#
#44307.4 pb-1
#2017 pp collisions dataset (fast data reprocessing)
#Tag pro22-01 (data periods B,C,D,E,F,H,I,K, fast data reprocessing) Full dataset of 2017 pp collisions is copied in https://atlas-groupdata.web.cern.ch/atlas-groupdata/GoodRunsLists/data17_13TeV/20180619/
#
#32988.1 pb-1
#Full 2016 pp collisions dataset (reprocessed with release 21)
#Tag pro21-01 (data periods A-L, reprocessed with release 21) full dataset of 2016 pp collisions copied in https://atlas-groupdata.web.cern.ch/atlas-groupdata/GoodRunsLists/data16_13TeV/20180129/
#
#3219.56 pb-1 
#Full 2015 pp collisions dataset (reprocessed with release 21)
#Tag pro21-02 (Periods D3 - J) full dataset of 2015 pp collisions copied in https://atlas-groupdata.web.cern.ch/atlas-groupdata/GoodRunsLists/data15_13TeV/20170619/
#
