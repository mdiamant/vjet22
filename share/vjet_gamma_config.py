# calibration
alg_selgam.calibration.ESModel = 'es2018_R21_v0'
alg_selgam.calibration.randomRunNumber = 123456

# selection
alg_selgam.pTmin = 25 # GeV
alg_selgam.etamax = 1.37
from ElectronPhotonSelectorTools import TrigEGammaPIDdefs
alg_selgam.selector.isEMMask = TrigEGammaPIDdefs.SelectionDefPhoton.PhotonTight
alg_selgam.selector.ConfigFile = 'ElectronPhotonSelectorTools/offline/20180116/PhotonIsEMTightSelectorCutDefs.conf'
alg_selgam.isolation.PhotonWP = 'FixedCutTight'
