#
# https://twiki.cern.ch/twiki/bin/view/Atlas/LowestUnprescaled

### ### ###
# strategy of avoiding pre-scaled triggers
### ### ###
##alg_selevt.triglist2015 = ['HLT_2e12_lhloose_L12EM10VH']
##alg_selevt.triglist2016 = ['HLT_2e17_lhvloose_nod0']
#alg_selevt.triglist201516 = ['HLT_2e12_lhloose_L12EM10VH','HLT_2e17_lhvloose_nod0']
## HLT_2e17_lhvloose_nod0_L12EM15VHI 2017 is accidentally prescaleed DO NOT use it
#alg_selevt.triglist2017 = ['HLT_2e24_lhvloose_nod0']
## due to SF ... given 2017 has no VHI then 2018 drop VHI
#alg_selevt.triglist2018 = ['HLT_2e24_lhvloose_nod0','HLT_2e24_lhvloose_nod0_L1_2EM20VH']

### ### ###
# strategy of being historical consistent and not changing offline lepton pT thresholds (20GeV)
### ### ###
# if avoid accidentially pre-scaled triggers, the threshold in triggers would be higher than 20GeV
# given the fact only very small fraction of evets were accidentically pre-scaled
# and the fact that no event counting in data or MC will be used directly, but only the response
# go with historical triggers
# https://cds.cern.ch/record/2669655/files/ATL-COM-PHYS-2019-281.pdf
alg_selevt.triglist201516 = ['HLT_2e12_lhloose_L12EM10VH','HLT_2e15_lhvloose_nod0_L12EM13VH']
alg_selevt.triglist2017 = ['HLT_2e17_lhvloose_nod0_L12EM15VHI']
alg_selevt.triglist2018 = ['HLT_2e17_lhvloose_nod0_L12EM15VHI']

