// SO FAR only read the efficiencies DO NOT look at the error of them !!!

#include "tab_xsecweight.C"

map<int, string> map_dsid_fnm;

void init_map_dsid_fnm(){

  // these correspond grid running May 2020 gridout/
map_dsid_fnm[364128] = "gridout/user.xiaohu.lu.testv16ade.364128.e5307_s3126_r9364_p4097_hist/merged.root";
map_dsid_fnm[364129] = "gridout/user.xiaohu.lu.testv16ade.364129.e5307_s3126_r9364_p4097_hist/merged.root";
map_dsid_fnm[364130] = "gridout/user.xiaohu.lu.testv16ade.364130.e5307_s3126_r9364_p4097_hist/merged.root";
map_dsid_fnm[364131] = "gridout/user.xiaohu.lu.testv16ade.364131.e5307_s3126_r9364_p4097_hist/merged.root";
map_dsid_fnm[364132] = "gridout/user.xiaohu.lu.testv16ade.364132.e5307_s3126_r9364_p4097_hist/merged.root";
map_dsid_fnm[364133] = "gridout/user.xiaohu.lu.testv16ade.364133.e5307_s3126_r9364_p4097_hist/merged.root";
map_dsid_fnm[364134] = "gridout/user.xiaohu.lu.testv16ade.364134.e5307_s3126_r9364_p4097_hist/merged.root";
map_dsid_fnm[364135] = "gridout/user.xiaohu.lu.testv16ade.364135.e5307_s3126_r9364_p4097_hist/merged.root";
map_dsid_fnm[364136] = "gridout/user.xiaohu.lu.testv16ade.364136.e5307_s3126_r9364_p4097_hist/merged.root";
map_dsid_fnm[364137] = "gridout/user.xiaohu.lu.testv16ade.364137.e5307_s3126_r9364_p4097_hist/merged.root";
map_dsid_fnm[364138] = "gridout/user.xiaohu.lu.testv16ade.364138.e5313_s3126_r9364_p4097_hist/merged.root";
map_dsid_fnm[364139] = "gridout/user.xiaohu.lu.testv16ade.364139.e5313_s3126_r9364_p4097_hist/merged.root";
map_dsid_fnm[364140] = "gridout/user.xiaohu.lu.testv16ade.364140.e5307_s3126_r9364_p4097_hist/merged.root";
map_dsid_fnm[364141] = "gridout/user.xiaohu.lu.testv16ade.364141.e5307_s3126_r9364_p4097_hist/merged.root";

}

// add h0 + c * hadd, hadd might have few bins than h0
void add( TH1* h0, TH1* hadd, double c ){

  for( unsigned int i = 1; i <= h0->GetNbinsX(); ++i ){
    const char* _label = h0->GetXaxis()->GetBinLabel(i);
	double _c_h0 = h0->GetBinContent( i );
	double _err_h0 = h0->GetBinError( i );
	double _c_hadd = 0;
	double _err_hadd = 0;
	for( unsigned int ihadd = 1; ihadd <= hadd->GetNbinsX(); ++ihadd ){
	  const char* _label_hadd = hadd->GetXaxis()->GetBinLabel( ihadd );
	  if( string(_label_hadd) == string(_label) ){
	    _c_hadd = hadd->GetBinContent(ihadd);
		_err_hadd = hadd->GetBinError(ihadd);
	  }
	}
	h0->SetBinContent( i, _c_h0 + c * _c_hadd );
	h0->SetBinError( i, TMath::Sqrt( _err_h0*_err_h0 + c*c*_err_hadd*_err_hadd) );
  }

}

void mergeSherpaHist()
{

   init_tab_xsecweight();
   init_map_dsid_fnm();

   // Create a new file
   auto newfile = new TFile("Sherpa_221_NNPDF30NNLO_Ztautau_mergedhist.root", "recreate");

   // Sherpa histograms have cutflow with different bins due to low stats in some sliced
   // Use Powheg histograms as templates
   auto templatefile = new TFile("gridout/user.xiaohu.lu.testv10.361108.e3601_s3126_r9364_p3975_hist/merged.root");
   //auto templatefile = new TFile("output_Zll-361108.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Ztautau/hist-mc16_13TeV.361108.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Ztautau.deriv.DAOD_STDM4.e3601_s3126_r9364_p3975.root");

   TH1* _h_Ztautau_etau_1prong_cutflow_weight  = (TH1*) ((TH1*)templatefile->Get("Ztautau_etau_1prong_cutflow_weight"))->Clone();
   TH1* _h_Ztautau_etau_3prong_cutflow_weight  = (TH1*) ((TH1*)templatefile->Get("Ztautau_etau_3prong_cutflow_weight"))->Clone();
   TH1* _h_Ztautau_mutau_1prong_cutflow_weight = (TH1*) ((TH1*)templatefile->Get("Ztautau_mutau_1prong_cutflow_weight"))->Clone();
   TH1* _h_Ztautau_mutau_3prong_cutflow_weight = (TH1*) ((TH1*)templatefile->Get("Ztautau_mutau_3prong_cutflow_weight"))->Clone();
   TH1* _h_Ztautau_emu_cutflow_weight          = (TH1*) ((TH1*)templatefile->Get("Ztautau_emu_cutflow_weight"))->Clone();
   //
   _h_Ztautau_etau_1prong_cutflow_weight ->SetDirectory(newfile);
   _h_Ztautau_etau_3prong_cutflow_weight ->SetDirectory(newfile);
   _h_Ztautau_mutau_1prong_cutflow_weight->SetDirectory(newfile);
   _h_Ztautau_mutau_3prong_cutflow_weight->SetDirectory(newfile);
   _h_Ztautau_emu_cutflow_weight         ->SetDirectory(newfile);
   //
   _h_Ztautau_etau_1prong_cutflow_weight ->Reset();
   _h_Ztautau_etau_3prong_cutflow_weight ->Reset();
   _h_Ztautau_mutau_1prong_cutflow_weight->Reset();
   _h_Ztautau_mutau_3prong_cutflow_weight->Reset();
   _h_Ztautau_emu_cutflow_weight         ->Reset();

   bool isfirst = true;
   for( auto const& x: map_dsid_fnm ){

     int dsid = x.first;
     cout << "Proccessing " << dsid << endl;
     TFile _infile(x.second.c_str());

     add( _h_Ztautau_etau_1prong_cutflow_weight ,  (TH1*) (_infile.Get("Ztautau_etau_1prong_cutflow_weight")), map_dsid_xsecw[dsid]);
     add( _h_Ztautau_etau_3prong_cutflow_weight ,  (TH1*) (_infile.Get("Ztautau_etau_3prong_cutflow_weight")), map_dsid_xsecw[dsid]);
     add( _h_Ztautau_mutau_1prong_cutflow_weight,  (TH1*) (_infile.Get("Ztautau_mutau_1prong_cutflow_weight")),map_dsid_xsecw[dsid]);
     add( _h_Ztautau_mutau_3prong_cutflow_weight,  (TH1*) (_infile.Get("Ztautau_mutau_3prong_cutflow_weight")),map_dsid_xsecw[dsid]);
     add( _h_Ztautau_emu_cutflow_weight         ,  (TH1*) (_infile.Get("Ztautau_emu_cutflow_weight")),         map_dsid_xsecw[dsid]);
	 
   }

   newfile->cd();
   newfile->Write();
}


