// get sum of events weight for all events in MC
#include <limits>
typedef std::numeric_limits< double > dbl;

double _get_sumw( const char* fnm ){
  TFile f( fnm );
  return ((TH1*) f.Get("selevt_cutflow_weight"))->GetBinContent(1);
}

void get_sumw(){

  map<int, string> map_dsid_fnm;
  // these correspond grid running latest 2020 gridout/
  // !!! MC16a !!!
  //map_dsid_fnm[364114] = "gridout/user.xiaohu.vjet.ZeeMC16aV3.364114.364114.e5299_s3126_r9364_p4060_hist/merged.root";
  //map_dsid_fnm[364115] = "gridout/user.xiaohu.vjet.ZeeMC16aV3.364115.364115.e5299_s3126_r9364_p4049_hist/merged.root";
  //map_dsid_fnm[364116] = "gridout/user.xiaohu.vjet.ZeeMC16aV3.364116.364116.e5299_s3126_r9364_p4060_hist/merged.root";
  //map_dsid_fnm[364117] = "gridout/user.xiaohu.vjet.ZeeMC16aV3.364117.364117.e5299_s3126_r9364_p4060_hist/merged.root";
  //map_dsid_fnm[364118] = "gridout/user.xiaohu.vjet.ZeeMC16aV3.364118.364118.e5299_s3126_r9364_p4049_hist/merged.root";
  //map_dsid_fnm[364119] = "gridout/user.xiaohu.vjet.ZeeMC16aV3.364119.364119.e5299_s3126_r9364_p4049_hist/merged.root";
  //map_dsid_fnm[364120] = "gridout/user.xiaohu.vjet.ZeeMC16aV3.364120.364120.e5299_s3126_r9364_p4049_hist/merged.root";
  //map_dsid_fnm[364121] = "gridout/user.xiaohu.vjet.ZeeMC16aV3.364121.364121.e5299_s3126_r9364_p4049_hist/merged.root";
  //map_dsid_fnm[364122] = "gridout/user.xiaohu.vjet.ZeeMC16aV3.364122.364122.e5299_s3126_r9364_p4049_hist/merged.root";
  //map_dsid_fnm[364123] = "gridout/user.xiaohu.vjet.ZeeMC16aV3.364123.364123.e5299_s3126_r9364_p4049_hist/merged.root";
  //map_dsid_fnm[364124] = "gridout/user.xiaohu.vjet.ZeeMC16aV3.364124.364124.e5299_s3126_r9364_p4049_hist/merged.root";
  //map_dsid_fnm[364125] = "gridout/user.xiaohu.vjet.ZeeMC16aV3.364125.364125.e5299_s3126_r9364_p4049_hist/merged.root";
  //map_dsid_fnm[364126] = "gridout/user.xiaohu.vjet.ZeeMC16aV3.364126.364126.e5299_s3126_r9364_p4049_hist/merged.root";
  //map_dsid_fnm[364127] = "gridout/user.xiaohu.vjet.ZeeMC16aV3.364127.364127.e5299_s3126_r9364_p4049_hist/merged.root";
  // !!! MC16d !!!
  //map_dsid_fnm[364114] = "gridout/user.xiaohu.vjet.ZeeMC16dV3.364114.364114.e5299_s3126_r10201_p4049_hist/merged.root";
  //map_dsid_fnm[364115] = "gridout/user.xiaohu.vjet.ZeeMC16dV3.364115.364115.e5299_s3126_r10201_p4049_hist/merged.root";
  //map_dsid_fnm[364116] = "gridout/user.xiaohu.vjet.ZeeMC16dV3.364116.364116.e5299_s3126_r10201_p4049_hist/merged.root";
  //map_dsid_fnm[364117] = "gridout/user.xiaohu.vjet.ZeeMC16dV3.364117.364117.e5299_s3126_r10201_p4049_hist/merged.root";
  //map_dsid_fnm[364118] = "gridout/user.xiaohu.vjet.ZeeMC16dV3.364118.364118.e5299_s3126_r10201_p4049_hist/merged.root";
  //map_dsid_fnm[364119] = "gridout/user.xiaohu.vjet.ZeeMC16dV3.364119.364119.e5299_s3126_r10201_p4049_hist/merged.root";
  //map_dsid_fnm[364120] = "gridout/user.xiaohu.vjet.ZeeMC16dV3.364120.364120.e5299_s3126_r10201_p4049_hist/merged.root";
  //map_dsid_fnm[364121] = "gridout/user.xiaohu.vjet.ZeeMC16dV3.364121.364121.e5299_s3126_r10201_p4049_hist/merged.root";
  //map_dsid_fnm[364122] = "gridout/user.xiaohu.vjet.ZeeMC16dV3.364122.364122.e5299_s3126_r10201_p4049_hist/merged.root";
  //map_dsid_fnm[364123] = "gridout/user.xiaohu.vjet.ZeeMC16dV3.364123.364123.e5299_s3126_r10201_p4049_hist/merged.root";
  //map_dsid_fnm[364124] = "gridout/user.xiaohu.vjet.ZeeMC16dV3.364124.364124.e5299_s3126_r10201_p4049_hist/merged.root";
  //map_dsid_fnm[364125] = "gridout/user.xiaohu.vjet.ZeeMC16dV3.364125.364125.e5299_s3126_r10201_p4049_hist/merged.root";
  //map_dsid_fnm[364126] = "gridout/user.xiaohu.vjet.ZeeMC16dV3.364126.364126.e5299_s3126_r10201_p4049_hist/merged.root";
  //map_dsid_fnm[364127] = "gridout/user.xiaohu.vjet.ZeeMC16dV3.364127.364127.e5299_s3126_r10201_p4060_hist/merged.root";
  // !!! MC16e !!!
  map_dsid_fnm[364114] = "gridout/user.xiaohu.vjet.ZeeMC16eV3.364114.364114.e5299_s3126_r10724_p4060_hist/merged.root";
  map_dsid_fnm[364115] = "gridout/user.xiaohu.vjet.ZeeMC16eV3.364115.364115.e5299_s3126_r10724_p4060_hist/merged.root";
  map_dsid_fnm[364116] = "gridout/user.xiaohu.vjet.ZeeMC16eV3.364116.364116.e5299_s3126_r10724_p4060_hist/merged.root";
  map_dsid_fnm[364117] = "gridout/user.xiaohu.vjet.ZeeMC16eV3.364117.364117.e5299_s3126_r10724_p4049_hist/merged.root";
  map_dsid_fnm[364118] = "gridout/user.xiaohu.vjet.ZeeMC16eV3.364118.364118.e5299_s3126_r10724_p4049_hist/merged.root";
  map_dsid_fnm[364119] = "gridout/user.xiaohu.vjet.ZeeMC16eV3.364119.364119.e5299_s3126_r10724_p4049_hist/merged.root";
  map_dsid_fnm[364120] = "gridout/user.xiaohu.vjet.ZeeMC16eV3.364120.364120.e5299_s3126_r10724_p4049_hist/merged.root";
  map_dsid_fnm[364121] = "gridout/user.xiaohu.vjet.ZeeMC16eV3.364121.364121.e5299_s3126_r10724_p4049_hist/merged.root";
  map_dsid_fnm[364122] = "gridout/user.xiaohu.vjet.ZeeMC16eV3.364122.364122.e5299_s3126_r10724_p4049_hist/merged.root";
  map_dsid_fnm[364123] = "gridout/user.xiaohu.vjet.ZeeMC16eV3.364123.364123.e5299_s3126_r10724_p4049_hist/merged.root";
  map_dsid_fnm[364124] = "gridout/user.xiaohu.vjet.ZeeMC16eV3.364124.364124.e5299_s3126_r10724_p4049_hist/merged.root";
  map_dsid_fnm[364125] = "gridout/user.xiaohu.vjet.ZeeMC16eV3.364125.364125.e5299_s3126_r10724_p4049_hist/merged.root";
  map_dsid_fnm[364126] = "gridout/user.xiaohu.vjet.ZeeMC16eV3.364126.364126.e5299_s3126_r10724_p4060_hist/merged.root";
  map_dsid_fnm[364127] = "gridout/user.xiaohu.vjet.ZeeMC16eV3.364127.364127.e5299_s3126_r10724_p4049_hist/merged.root";

  cout << "DSID SUMW" << endl;
  cout.precision(dbl::max_digits10);
  for( auto const& x : map_dsid_fnm ){
    cout << x.first << " " << _get_sumw( x.second.c_str() ) << endl;
  }
}


