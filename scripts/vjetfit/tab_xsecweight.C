// use HGamSamples to get xsec kfactor etc. xsec-lu/xs_mc16_lu.txt
// use get_sumw.C to get sumw of all MC events
// manually copy and multiply here
//
map<int, double> map_dsid_xsecw;
double lumi = 139*1000; // ifb, this does not matter for truth studies

void init_tab_xsecweight( const char* mc ){

///////////////////////////////////
// 2020 adapted to vjet calibration
///////////////////////////////////

// obtain this from HGamSamples/xsec-vjet/xs_mc16_vjet.txt
// crossSection*kFactor*genFiltEff / sumW * lumi

// obtain the sum_of_weight from get_sumw
//
// mc16a 20200716
if( TString(mc) == "MC16a" ){
map_dsid_xsecw[364114] = 1.9816E+00 * 0.9751 * 8.2118E-01 / 1566306.125  * lumi;
map_dsid_xsecw[364115] = 1.9817E+00 * 0.9751 * 1.1347E-01 / 892765.375   * lumi;
map_dsid_xsecw[364116] = 1.9820E+00 * 0.9751 * 6.5194E-02 / 1424412.75   * lumi;
map_dsid_xsecw[364117] = 1.1064E-01 * 0.9751 * 6.9275E-01 / 758490.6875  * lumi;
map_dsid_xsecw[364118] = 1.1050E-01 * 0.9751 * 1.8933E-01 / 264624.46875 * lumi;
map_dsid_xsecw[364119] = 1.1046E-01 * 0.9751 * 1.1547E-01 / 781990.4375  * lumi;
map_dsid_xsecw[364120] = 4.0645E-02 * 0.9751 * 6.1591E-01 / 1116742.5    * lumi;
map_dsid_xsecw[364121] = 4.0671E-02 * 0.9751 * 2.3209E-01 / 774138       * lumi;
map_dsid_xsecw[364122] = 4.0675E-02 * 0.9751 * 1.5238E-01 / 3523513.25   * lumi;
map_dsid_xsecw[364123] = 8.6703E-03 * 0.9751 * 5.6349E-01 / 703820.1875  * lumi;
map_dsid_xsecw[364124] = 8.6668E-03 * 0.9751 * 2.6531E-01 / 379664.9375  * lumi;
map_dsid_xsecw[364125] = 8.6799E-03 * 0.9751 * 1.7578E-01 / 1611918.875  * lumi;
map_dsid_xsecw[364126] = 1.8092E-03 * 0.9751 * 1.0000E+00 / 1329219.875  * lumi;
map_dsid_xsecw[364127] = 1.4875E-04 * 0.9751 * 1.0000E+00 / 497662.9375  * lumi;
}
if( TString(mc) == "MC16d" ){
map_dsid_xsecw[364114] = 1.9816E+00 * 0.9751 * 8.2118E-01 / 1908092.5    * lumi;
map_dsid_xsecw[364115] = 1.9817E+00 * 0.9751 * 1.1347E-01 / 1105225.375  * lumi;
map_dsid_xsecw[364116] = 1.9820E+00 * 0.9751 * 6.5194E-02 / 1508454.125  * lumi;
map_dsid_xsecw[364117] = 1.1064E-01 * 0.9751 * 6.9275E-01 / 922393.75    * lumi;
map_dsid_xsecw[364118] = 1.1050E-01 * 0.9751 * 1.8933E-01 / 325516.5625  * lumi;
map_dsid_xsecw[364119] = 1.1046E-01 * 0.9751 * 1.1547E-01 / 995345.75    * lumi;
map_dsid_xsecw[364120] = 4.0645E-02 * 0.9751 * 6.1591E-01 / 1373051.875  * lumi;
map_dsid_xsecw[364121] = 4.0671E-02 * 0.9751 * 2.3209E-01 / 952910.875   * lumi;
map_dsid_xsecw[364122] = 4.0675E-02 * 0.9751 * 1.5238E-01 / 4363521      * lumi;
map_dsid_xsecw[364123] = 8.6703E-03 * 0.9751 * 5.6349E-01 / 873825.875   * lumi;
map_dsid_xsecw[364124] = 8.6668E-03 * 0.9751 * 2.6531E-01 / 468310       * lumi;
map_dsid_xsecw[364125] = 8.6799E-03 * 0.9751 * 1.7578E-01 / 1988617      * lumi;
map_dsid_xsecw[364126] = 1.8092E-03 * 0.9751 * 1.0000E+00 / 1410823.875  * lumi;
map_dsid_xsecw[364127] = 1.4875E-04 * 0.9751 * 1.0000E+00 / 619047.5625  * lumi;
}
if( TString(mc) == "MC16e" ){
map_dsid_xsecw[364114] = 1.9816E+00 * 0.9751 * 8.2118E-01 / 2538230.5    * lumi;
map_dsid_xsecw[364115] = 1.9817E+00 * 0.9751 * 1.1347E-01 / 1476830      * lumi;
map_dsid_xsecw[364116] = 1.9820E+00 * 0.9751 * 6.5194E-02 / 2315502.5    * lumi;
map_dsid_xsecw[364117] = 1.1064E-01 * 0.9751 * 6.9275E-01 / 1243438.375  * lumi;
map_dsid_xsecw[364118] = 1.1050E-01 * 0.9751 * 1.8933E-01 / 436954.375   * lumi;
map_dsid_xsecw[364119] = 1.1046E-01 * 0.9751 * 1.1547E-01 / 1309692.75   * lumi;
map_dsid_xsecw[364120] = 4.0645E-02 * 0.9751 * 6.1591E-01 / 1852873.875  * lumi;
map_dsid_xsecw[364121] = 4.0671E-02 * 0.9751 * 2.3209E-01 / 1254121.5    * lumi;
map_dsid_xsecw[364122] = 4.0675E-02 * 0.9751 * 1.5238E-01 / 5784695.5    * lumi;
map_dsid_xsecw[364123] = 8.6703E-03 * 0.9751 * 5.6349E-01 / 1197692.75   * lumi;
map_dsid_xsecw[364124] = 8.6668E-03 * 0.9751 * 2.6531E-01 / 652310.5625  * lumi;
map_dsid_xsecw[364125] = 8.6799E-03 * 0.9751 * 1.7578E-01 / 2641514.5    * lumi;
map_dsid_xsecw[364126] = 1.8092E-03 * 0.9751 * 1.0000E+00 / 2196230.75   * lumi;
map_dsid_xsecw[364127] = 1.4875E-04 * 0.9751 * 1.0000E+00 / 830122.0625  * lumi;
}

}

