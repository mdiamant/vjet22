int hist_Zjets( const char* fnm, const char* outfnm ){

  TFile treefile( fnm );
  TTreeReader reader( "vjet", &treefile );
  TTreeReaderValue<double> weight(reader, "weight");
  TTreeReaderValue<double> weight_mc(reader, "weight_mc");
  TTreeReaderValue<double> weight_pileup(reader, "weight_pileup");
  TTreeReaderValue<double> weight_lepeff_trig(reader, "weight_lepeff_trig");
  TTreeReaderValue<double> weight_lepeff_reco(reader, "weight_lepeff_reco");
  TTreeReaderValue<double> weight_lepeff_id(reader, "weight_lepeff_id");
  TTreeReaderValue<double> weight_lepeff_iso(reader, "weight_lepeff_iso");
  //
  TTreeReaderValue<int> pass_all(reader, "pass_all");
  TTreeReaderValue<int> pass_grl        (reader, "pass_grl");
  TTreeReaderValue<int> pass_trigger    (reader, "pass_trigger");
  TTreeReaderValue<int> pass_trigmatch  (reader, "pass_trigmatch");
  TTreeReaderValue<int> pass_primary_vertex(reader, "pass_primary_vertex");
  TTreeReaderValue<int> pass_dq         (reader, "pass_dq");
  TTreeReaderValue<int> pass_jetclean   (reader, "pass_jetclean");
  TTreeReaderValue<int> pass_twolepton  (reader, "pass_twolepton");
  TTreeReaderValue<int> pass_mll        (reader, "pass_mll");
  TTreeReaderValue<int> pass_onejet     (reader, "pass_onejet");
  TTreeReaderValue<int> pass_j0pt       (reader, "pass_j0pt");
  TTreeReaderValue<int> pass_j0eta      (reader, "pass_j0eta");
  TTreeReaderValue<int> pass_dphi_jZ    (reader, "pass_dphi_jZ");
  TTreeReaderValue<int> pass_j1pt       (reader, "pass_j1pt");
  //
  TTreeReaderValue<double> lep0_pt  (reader, "lep0_pt");
  TTreeReaderValue<double> lep0_eta (reader, "lep0_eta");
  TTreeReaderValue<double> lep1_pt  (reader, "lep1_pt");
  TTreeReaderValue<double> lep1_eta (reader, "lep1_eta");
  TTreeReaderValue<double> z_pt     (reader, "z_pt");
  TTreeReaderValue<double> z_eta    (reader, "z_eta");
  TTreeReaderValue<double> z_m      (reader, "z_m");
  TTreeReaderValue<int> jet_n    (reader, "jet_n");
  TTreeReaderValue<double> jet0_pt  (reader, "jet0_pt");
  TTreeReaderValue<double> jet0_eta (reader, "jet0_eta");
  TTreeReaderValue<double> jet1_pt  (reader, "jet1_pt");
  TTreeReaderValue<double> jet1_eta (reader, "jet1_eta");
  TTreeReaderValue<double> dphi_j0_z(reader, "dphi_j0_z");
  TTreeReaderValue<double> met      (reader, "met");
  TTreeReaderValue<double> resp_db  (reader, "resp_db");
  TTreeReaderValue<double> resp_mpf (reader, "resp_mpf");

  //
  TFile outfile( outfnm,"RECREATE");
  TH1F* h_weight   = new TH1F("weight","weight", 20000, -10000, 10000 );
  TH1F* h_weight_mc = new TH1F("weight_mc","weight_mc", 20000, -10000, 10000 );
  TH1F* h_weight_pileup   = new TH1F("weight_pileup","weight_pileup", 20000, -10000, 10000 );
  TH1F* h_weight_lepeff_trig   = new TH1F("weight_lepeff_trig","weight_lepeff_trig", 20000, -10000, 10000 );
  TH1F* h_weight_lepeff_reco   = new TH1F("weight_lepeff_reco","weight_lepeff_reco", 20000, -10000, 10000 );
  TH1F* h_weight_lepeff_id   = new TH1F("weight_lepeff_id","weight_lepeff_id", 20000, -10000, 10000 );
  TH1F* h_weight_lepeff_iso   = new TH1F("weight_lepeff_iso","weight_lepeff_iso", 20000, -10000, 10000 );

  TH1F* h_lep0_pt  = new TH1F("lep0_pt  ","lep0_pt  ",150,0,150);
  TH1F* h_lep0_eta = new TH1F("lep0_eta ","lep0_eta ",150,-3,3);
  TH1F* h_lep1_pt  = new TH1F("lep1_pt  ","lep1_pt  ",150,0,150);
  TH1F* h_lep1_eta = new TH1F("lep1_eta ","lep1_eta ",150,-3,3);
  TH1F* h_z_pt     = new TH1F("z_pt     ","z_pt     ",200,0,200);
  TH1F* h_z_eta    = new TH1F("z_eta    ","z_eta    ",200,-5,5);
  TH1F* h_z_m      = new TH1F("z_m      ","z_m      ",130,0,130);
  TH1F* h_jet_n    = new TH1F("jet_n    ","jet_n    ",10,0,10);
  TH1F* h_jet0_pt  = new TH1F("jet0_pt  ","jet0_pt  ",150,0,150);
  TH1F* h_jet0_eta = new TH1F("jet0_eta ","jet0_eta ",200,-5,5);
  TH1F* h_jet1_pt  = new TH1F("jet1_pt  ","jet1_pt  ",150,0,150);
  TH1F* h_jet1_eta = new TH1F("jet1_eta ","jet1_eta ",200,-5,5);
  TH1F* h_dphi_j0_z= new TH1F("dphi_j0_z","dphi_j0_z",315,0,3.15);
  TH1F* h_met      = new TH1F("met      ","met      ",150,0,150);
  TH1F* h_resp_db  = new TH1F("resp_db  ","resp_db  ",200,0.,2.0);
  TH1F* h_resp_mpf = new TH1F("resp_mpf ","resp_mpf ",200,0.,2.0);

  long int _ctr_pass = 0;

  while (reader.Next()) {

    //if( (*pass_all) ) ;
	//
	// corrected pass_all
	if(
      (* pass_grl        )*
      (* pass_trigger    )*
      (* pass_trigmatch  )*
      (* pass_primary_vertex)*
      (* pass_dq         )*
      (* pass_jetclean   )*
      (* pass_twolepton  )*
      (* pass_mll        )*
      (* pass_onejet     )*
      (* pass_j0pt       )*
      (* pass_j0eta      )*
      (* pass_dphi_jZ    )
    ){
      ;

      bool _pass_j1pt = false;

	  if( (0.1 * (*z_pt)) > 15 ){
	    if( (*jet1_pt) < (0.1 * (*z_pt)) ) _pass_j1pt = true;
	  }
	  else{
	    if( (*jet1_pt) < 15 ) _pass_j1pt = true;
	  }

      if( _pass_j1pt ) ;
	  else continue;

    }
	else continue;

	// test pt bins
    double _pTref = (*z_pt) * TMath::Abs(TMath::Cos(*dphi_j0_z));
	if( _pTref > 60 && _pTref < 80 );
	else continue;

    // cut studies
	//if( (*pass_grl)*(*pass_trigger)*(*pass_trigmatch)*(*pass_primary_vertex)*(*pass_dq) // no impact on ZpT: single peak at 0
	//    *(*pass_jetclean)*(*pass_twolepton)*(*pass_twolepton) // no impact on ZpT: single peak at 0
	//	*(*pass_onejet)*(*pass_j0pt)*(*pass_j0eta) // no impact on ZpT: single peak at 0
	//	*(*pass_dphi_jZ) // move ZpT peak to 20 GeV
	//	*(*pass_j1pt) // !!! ZpT peaj at 20 GeV, then second peak at 60 GeV !!!
	//	) ;
    //else continue;

    h_weight->Fill( *weight );
    h_weight_mc->Fill( *weight_mc );
    h_weight_pileup->Fill( *weight_pileup );
    h_weight_lepeff_trig->Fill( *weight_lepeff_trig );
    h_weight_lepeff_reco->Fill( *weight_lepeff_reco );
    h_weight_lepeff_id->Fill( *weight_lepeff_id );
    h_weight_lepeff_iso->Fill( *weight_lepeff_iso );

    h_lep0_pt  ->Fill( *lep0_pt  , *weight );
    h_lep0_eta ->Fill( *lep0_eta , *weight );
    h_lep1_pt  ->Fill( *lep1_pt  , *weight );
    h_lep1_eta ->Fill( *lep1_eta , *weight );
    h_z_pt     ->Fill( *z_pt     , *weight );
    h_z_eta    ->Fill( *z_eta    , *weight );
    h_z_m      ->Fill( *z_m      , *weight );
    h_jet_n    ->Fill( *jet_n    , *weight );
    h_jet0_pt  ->Fill( *jet0_pt  , *weight );
    h_jet0_eta ->Fill( *jet0_eta , *weight );
    h_jet1_pt  ->Fill( *jet1_pt  , *weight );
    h_jet1_eta ->Fill( *jet1_eta , *weight );
    h_dphi_j0_z->Fill( *dphi_j0_z, *weight );
    h_met      ->Fill( *met      , *weight );
    h_resp_db  ->Fill( *resp_db  , *weight );
    h_resp_mpf ->Fill( *resp_mpf , *weight );

	//if( _ctr_pass == 200000 ) break;
	++_ctr_pass;

  }

  // to disk
  outfile.Write();
  outfile.Close();

  return 0;
}
