#################
# fit, draw and save responses
#################
#rbq binned_fit.C'("test_samples.txt","vjet","test_mc16a","MPF")' &
#rbq binned_fit.C'("test_samples.txt","vjet","test_mc16a_simpleORdR035","DB")' &
# 2020 07 16
#rbq binned_fit.C'("list/ZeePhPy8_mc16a.txt","vjet","ZeePhPy8_mc16a","DB")' &
#rbq binned_fit.C'("list/ZeePhPy8_mc16d.txt","vjet","ZeePhPy8_mc16d","DB")'
#rbq binned_fit.C'("list/ZeePhPy8_mc16e.txt","vjet","ZeePhPy8_mc16e","DB")'
#rbq binned_fit.C'("list/ZeeSherpa221_mc16a.txt","vjet","ZeeSherpa221_mc16a","DB")' &
#rbq binned_fit.C'("list/ZeeSherpa221_mc16d.txt","vjet","ZeeSherpa221_mc16d","DB")' &
#rbq binned_fit.C'("list/ZeeSherpa221_mc16e.txt","vjet","ZeeSherpa221_mc16e","DB")'
#rbq binned_fit.C'("list/data_1516.txt","vjet","data_1516","DB")'
#rbq binned_fit.C'("list/data_17.txt","vjet","data_17","DB")'
#rbq binned_fit.C'("list/data_18.txt","vjet","data_18","DB")'

#################
# kinematics
#################
#rbq hist_Zjets.C'("gridout/user.xiaohu.vjet.ZeeMC16aV4newOR.361106.361106.e3601_s3126_r9364_p4060_ANALYSIS.root/merged.root","kinematics/test_mc16a_simpleORdR035.root")'
#rbq hist_Zjets.C'("gridout/user.xiaohu.vjet.ZeeMC16aV3.361106.361106.e3601_s3126_r9364_p4060_ANALYSIS.root/merged.root ","kinematics/ZeePhPy8_mc16a_pTref_25_30.root")'
# 2020 07 16
#rbq hist_Zjets.C'("gridout/user.xiaohu.vjet.ZeeMC16aV3.361106.361106.e3601_s3126_r9364_p4060_ANALYSIS.root/merged.root ","kinematics/ZeePhPy8_mc16a.root")'
#rbq hist_Zjets.C'("gridout/user.xiaohu.vjet.ZeeMC16dV3.361106.361106.e3601_s3126_r10201_p4060_ANALYSIS.root/merged.root","kinematics/ZeePhPy8_mc16d.root")'
#rbq hist_Zjets.C'("gridout/user.xiaohu.vjet.ZeeMC16eV3.361106.361106.e3601_s3126_r10724_p4060_ANALYSIS.root/merged.root","kinematics/ZeePhPy8_mc16e.root")'
#rbq hist_Zjets.C'("Sherpa_221_Ztautau_mergedtree_MC16a.root","kinematics/ZeeSherpa221_mc16a.root")' &
#rbq hist_Zjets.C'("Sherpa_221_Ztautau_mergedtree_MC16d.root","kinematics/ZeeSherpa221_mc16d.root")' &
#rbq hist_Zjets.C'("Sherpa_221_Ztautau_mergedtree_MC16e.root","kinematics/ZeeSherpa221_mc16e.root")' &
#
#rbq hist_Zjets.C'("gridout/data-merged-1516.root","kinematics/data_1516.root")' &
#rbq hist_Zjets.C'("gridout/data-merged-17.root","kinematics/data_17.root")' &
#rbq hist_Zjets.C'("gridout/data-merged-18.root","kinematics/data_18.root")' &



#################
# fit&draw better
#################
#rbq fitndraw.C'("result_mc16adeNominal/ZeePhPy8_mc16a-DB-hist-pTref.root","ZeePhPy8_mc16a","DB")'
#rbq fitndraw.C'("result_mc16adeNominal/ZeePhPy8_mc16d-DB-hist-pTref.root","ZeePhPy8_mc16d","DB")'
#rbq fitndraw.C'("result_mc16adeNominal/ZeePhPy8_mc16e-DB-hist-pTref.root","ZeePhPy8_mc16e","DB")'
#rbq fitndraw.C'("result_mc16adeNominal/ZeeSherpa221_mc16a-DB-hist-pTref.root","ZeeSherpa221_mc16a","DB")'
#rbq fitndraw.C'("result_mc16adeNominal/ZeeSherpa221_mc16d-DB-hist-pTref.root","ZeeSherpa221_mc16d","DB")'
#rbq fitndraw.C'("result_mc16adeNominal/ZeeSherpa221_mc16e-DB-hist-pTref.root","ZeeSherpa221_mc16e","DB")'
#
#rbq fitndraw.C'("result_data15161718/data_1516-DB-hist-pTref.root","data_1516","DB")'
#rbq fitndraw.C'("result_data15161718/data_17-DB-hist-pTref.root","data_17","DB")'
#rbq fitndraw.C'("result_data15161718/data_18-DB-hist-pTref.root","data_18","DB")'

# test muon
#rbq fitndraw.C'("result_mc16aZmumu/test_Zmm_mc16a-DB-hist-pTref.root","test_Zmm_mc16a","DB")'


#################
# merging Sherpa
#################
#rbq mergeSherpaTree.C'("MC16a")'
#rbq mergeSherpaTree.C'("MC16d")'
#rbq mergeSherpaTree.C'("MC16e")'



