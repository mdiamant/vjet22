#ifndef VjetTreeMaker_Pileup_H
#define VjetTreeMaker_Pileup_H

#include <AsgTools/AnaToolHandle.h>
#include <xAODBase/IParticleHelpers.h>
#include <AnaAlgorithm/AnaAlgorithm.h>
#include <xAODEventInfo/EventInfo.h>
#include <xAODEventInfo/EventInfoAuxContainer.h>

#include <AsgAnalysisInterfaces/IPileupReweightingTool.h>

namespace VjTM{

class Pileup : public EL::AnaAlgorithm
{
public:
  // this is a standard algorithm constructor
  Pileup (const std::string& name, ISvcLocator* pSvcLocator);

  // these are the functions inherited from Algorithm
  virtual StatusCode initialize () override;
  virtual StatusCode execute () override;
  virtual StatusCode finalize () override;

  ToolHandle<CP::IPileupReweightingTool> prw;

private:
  // Configuration, and any other types of variables go here.
};

} // namespace VjTM
#endif
