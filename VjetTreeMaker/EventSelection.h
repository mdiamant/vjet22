#ifndef VjetTreeMaker_EventSelection_H
#define VjetTreeMaker_EventSelection_H

#include <AnaAlgorithm/AnaAlgorithm.h>
#include <xAODEventInfo/EventInfo.h>
#include <TH1.h>

#include <AssociationUtils/OverlapRemovalInit.h>
#include <AssociationUtils/OverlapRemovalDefs.h>

#include <xAODCore/AuxContainerBase.h>
#include <xAODEgamma/ElectronContainer.h>
#include <xAODEgamma/PhotonContainer.h>
#include <xAODJet/JetContainer.h>
#include <xAODMissingET/MissingETAuxContainer.h>
#include <xAODMissingET/MissingETContainer.h>
#include <xAODMuon/MuonContainer.h>
#include <xAODTracking/VertexContainer.h>

#include <AsgAnalysisInterfaces/IGoodRunsListSelectionTool.h>
//#include <AsgAnalysisInterfaces/IPileupReweightingTool.h>

#include <TrigDecisionTool/TrigDecisionTool.h>
#include <TriggerMatchingTool/MatchingTool.h>
#include <TrigConfxAOD/xAODConfigTool.h>

#include <VjetTreeMaker/ObjectSelection.h>
#include <VjetTreeMaker/Electron.h>
#include <VjetTreeMaker/Muon.h>
#include <VjetTreeMaker/Gamma.h>
#include <VjetTreeMaker/MET.h>
#include <VjetTreeMaker/Cutflow.h>

namespace VjTM {

class descending_pt_class{
public:
  bool operator() ( const xAOD::IParticle* p1, const xAOD::IParticle* p2 ){
    return (p1->pt() > p2->pt());
  }
};

class EventSelection : public EL::AnaAlgorithm, public Cutflow
{
public:
  // this is a standard algorithm constructor
  EventSelection (const std::string& name, ISvcLocator* pSvcLocator);

  // these are the functions inherited from Algorithm
  virtual StatusCode initialize () override; // CALL ME explicitly in derived classes !!!
  virtual StatusCode execute () override; // CALL ME explicitly in derived classes !!!
  virtual StatusCode finalize () override; // CALL ME explicitly in derived classes !!!

  // GRL
  ToolHandle<IGoodRunsListSelectionTool> grl;
  // pileup reweighting moved to a standalone algo
  // as it is required by object eff sf correction before event selections
  //ToolHandle<CP::IPileupReweightingTool> prw;

  bool isZeeJet(){ return process == "ZeeJet"; }
  bool isZmmJet(){ return process == "ZmmJet"; }
  bool isGammaJet(){ return process == "GammaJet"; }

  descending_pt_class descending_pt;

  // properties
  std::string process;
  bool isdata;
  std::string year; // 201516, 2017, 2018
  const xAOD::EventInfo * eventInfo;

  // functions
  double m01( xAOD::IParticleContainer& coll );

  // data quality
  bool pass_alldq();
  bool passLAr();
  bool passTile();
  bool passCore();
  bool passBackground();
  bool passSCT();
  bool passVertex();

  // primaty vertex
  bool pass_primvertex();

  // trigger
  // really a hard time to apply the standard ToolHandle and python interface to these trigger tools !!!
  // go hard coded instead
  TrigConf::xAODConfigTool* trigconf;
  Trig::TrigDecisionTool* trigdec;
  Trig::MatchingTool* trigmatch;
  std::vector<std::string> triglist;
  std::vector<std::string> triglist201516;
  std::vector<std::string> triglist2017;
  std::vector<std::string> triglist2018;
  bool pass_trig();
  bool pass_trigmch( const xAOD::IParticleContainer* parts );
  const double trigdR = 0.1;

  // overlap removal assisting functions
  // dR is less than dRmax: return true
  template<class Keep> bool or_isdR( const Keep* k, const xAOD::IParticle* p1, const double& dRmax ) {
    if(dRmax < 0) return false;
    TLorentzVector _lv = p1->p4();
    for( auto ik : *k ) { 
      if( _lv.DeltaR(ik->p4()) < dRmax ) return true; 
    }
    return false;
  }

  template<class Keep, class Remove> void or_removal( const Keep* k, Remove* r, const double& dRmax ){
    for( auto it = r->begin(); it != r->end(); ){
      if( or_isdR(k, *it, dRmax) ) r->erase(it);
      else ++it;
    }
  }

  // https://gitlab.cern.ch/atlas/athena/blob/21.2/PhysicsAnalysis/AnalysisCommon/AssociationUtils/
  template<class T> void or_clean( T* container, const std::string outputLabel, const bool outputPassValue ) {
    if( container == nullptr ) return;
    const ort::outputAccessor_t or_accessor(outputLabel);
    for( auto it = container->begin(); it != container->end(); ){
      if( or_accessor(*(*it)) != outputPassValue ) container->erase(it);
      else ++it;
    }
  }

private:
  // Configuration, and any other types of variables go here.
  //float m_cutValue;
  //TTree *m_myTree;
  //TH1 *m_myHist;

};


} // namespace VjTM

#endif
