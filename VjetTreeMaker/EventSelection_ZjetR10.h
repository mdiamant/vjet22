#ifndef VjetTreeMaker_EventSelection_ZjetR10_H
#define VjetTreeMaker_EventSelection_ZjetR10_H

#include <AnaAlgorithm/AnaAlgorithm.h>
#include <xAODJet/JetContainer.h>
#include <xAODEventInfo/EventInfo.h>
#include <TH1.h>
#include <VjetTreeMaker/EventSelection.h>
#include <VjetTreeMaker/Tree_ZjetR10.h>

namespace VjTM{

class EventSelection_ZjetR10 : public EventSelection, public Tree_ZjetR10
{
public:
  // this is a standard algorithm constructor
  EventSelection_ZjetR10 (const std::string& name, ISvcLocator* pSvcLocator);

  virtual StatusCode initialize () ;
  virtual StatusCode execute () ;
  virtual StatusCode finalize () ;

  std::string elec_collection;
  std::string muon_collection;
  std::string jetR04_collection;
  std::string jetR10_collection;

private:
  // Configuration, and any other types of variables go here.
  //float m_cutValue;
  //TTree *m_myTree;
  //TH1 *m_myHist;

  // overlap removal
  // https://gitlab.cern.ch/atlas/athena/blob/21.2/PhysicsAnalysis/AnalysisCommon/AssociationUtils/
  const std::string or_outputLabel = "overlaps";
  const bool or_outputPassValue = false;
  // or_accessor(*obj) != or_outputPassValue : then obj is overlapped
  ORUtils::ToolBox or_toolbox;

};

} // namespace VjTM

#endif
