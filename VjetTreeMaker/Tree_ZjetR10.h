#ifndef VjetTreeMaker_Tree_ZjetR10_H
#define VjetTreeMaker_Tree_ZjetR10_H

#include <TTree.h>

namespace VjTM{

// define tree leaves, connect to local variables and manage them
// creation and filling of the tree should be handled exernally
class Tree_ZjetR10 {

public:

  Tree_ZjetR10() {;}
  void initialize_tree( TTree* tr );
  void initialize_leaves();

  // event basic info
  int runnumber = 0; ///< Run number
  int eventnumber = 0; ///< Event number

  // weights
  double weight = 1;
  //double weight_ electron muon SF etc.
  double weight_mc = 1; // matrix element
  double weight_pileup = 1;
  
  // flag of pass event selections
  int pass_all = 0;
  int pass_init = 0;
  int pass_grl = 0;
  int pass_trigger = 0;
  int pass_trigmatch = 0;
  int pass_primary_vertex = 0;
  int pass_dq = 0;
  int pass_jetclean = 0;
  //
  int pass_twolepton = 0;
  int pass_opposign = 0;
  int pass_mll = 0;
  int pass_onefatj = 0;
  int pass_dphi_fjz = 0;
  int pass_jpt = 0;

  // variables
  double lep0_pt = 0;
  double lep0_eta = 0;
  double lep0_phi = 0;
  double lep0_e = 0;

  double lep1_pt = 0;
  double lep1_eta = 0;
  double lep1_phi = 0;
  double lep1_e = 0;

  double z_pt = 0;
  double z_eta = 0;
  double z_phi = 0;
  double z_e = 0;
  double z_m = 0;

  int jet_n = 0;

  double jet0_pt = 0;
  double jet0_eta = 0;
  double jet0_phi = 0;
  double jet0_e = 0;

  double jet1_pt = 0;
  double jet1_eta = 0;
  double jet1_phi = 0;
  double jet1_e = 0;

  int fjet_n = 0;

  double fjet0_pt = 0;
  double fjet0_eta = 0;
  double fjet0_phi = 0;
  double fjet0_e = 0;

  double dphi_fj0_z = 0;

  double met_x = 0;
  double met_y = 0;
  double met_phi = 0;
  double met = 0;

  double resp_db = 1;
  double resp_mpf = 1;

private:

  TTree* thetree;

};


} // namespace VjTM

#endif
