#ifndef VjetTreeMaker_MET_H
#define VjetTreeMaker_MET_H

#include <VjetTreeMaker/ObjectSelection.h>
#include <xAODMissingET/MissingETAuxContainer.h>
#include <xAODMissingET/MissingETAssociationMap.h>
#include <xAODMissingET/MissingETContainer.h>
#include <METInterface/IMETSystematicsTool.h>
#include <METInterface/IMETMaker.h>
#include <METUtilities/METHelpers.h>

#include <xAODJet/JetContainer.h>
#include <xAODEgamma/ElectronContainer.h>
#include <xAODEgamma/PhotonContainer.h>
#include <xAODMuon/MuonContainer.h>

#include <AsgTools/AnaToolHandle.h>
#include <METInterface/IMETMaker.h>
#include <xAODCore/AuxContainerBase.h>

namespace VjTM{

class MET : public ObjectSelection
{
public:
  // this is a standard algorithm constructor
  MET (const std::string& name, ISvcLocator* pSvcLocator);

  // these are the functions inherited from Algorithm
  virtual StatusCode initialize () override;
  virtual StatusCode execute () override;
  virtual StatusCode finalize () override;

  ToolHandle<IMETMaker> calibration;

  std::string process; // ZeeJet ZmmJet GammaJet
  std::string jettype; // type of jets for MET calc

  // input containers for calculating MET
  std::string input_electron;
  std::string input_muon;
  std::string input_gamma;
  std::string input_jet;

private:
  // Configuration, and any other types of variables go here.
};

} // namespace VjTM
#endif
