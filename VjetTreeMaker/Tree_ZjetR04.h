#ifndef VjetTreeMaker_Tree_ZjetR04_H
#define VjetTreeMaker_Tree_ZjetR04_H

#include <TTree.h>

namespace VjTM{

// define tree leaves, connect to local variables and manage them
// creation and filling of the tree should be handled exernally
class Tree_ZjetR04 {

public:

  Tree_ZjetR04() {;}
  void initialize_tree( TTree* tr );
  void initialize_leaves();

  // event basic info
  int runnumber = 0; ///< Run number
  int eventnumber = 0; ///< Event number

  // weights
  double weight = 1;
  //double weight_ electron muon SF etc.
  double weight_mc = 1; // matrix element
  double weight_pileup = 1;
  double weight_lepeff_trig = 1;
  double weight_lepeff_reco = 1;
  double weight_lepeff_id = 1;
  double weight_lepeff_iso = 1;
  
  // flag of pass event selections
  int pass_all = 0;
  int pass_init = 0;
  int pass_grl = 0;
  int pass_trigger = 0;
  int pass_trigmatch = 0;
  int pass_primary_vertex = 0;
  int pass_dq = 0;
  int pass_jetclean = 0;
  int pass_twolepton = 0;
  int pass_llcharge = 0;
  int pass_mll = 0;
  int pass_onejet = 0;
  int pass_j0pt = 0;
  int pass_j0eta = 0;
  int pass_dphi_jZ = 0;
  int pass_j1pt = 0;

  // variables
  double lep0_pt = 0;
  double lep0_eta = 0;
  double lep0_phi = 0;
  double lep0_e = 0;

  double lep1_pt = 0;
  double lep1_eta = 0;
  double lep1_phi = 0;
  double lep1_e = 0;

  double z_pt = 0;
  double z_eta = 0;
  double z_phi = 0;
  double z_e = 0;
  double z_m = 0;

  int jet_n = 0;

  double jet0_pt = 0;
  double jet0_eta = 0;
  double jet0_phi = 0;
  double jet0_e = 0;

  double jet1_pt = 0;
  double jet1_eta = 0;
  double jet1_phi = 0;
  double jet1_e = 0;

  double dphi_j0_z = 0;

  double met_x = 0;
  double met_y = 0;
  double met_phi = 0;
  double met = 0;

  double resp_db = 1;
  double resp_mpf = 1;

private:

  TTree* thetree;

};


} // namespace VjTM

#endif
