#ifndef VjetTreeMaker_ObjectSelection_H
#define VjetTreeMaker_ObjectSelection_H

#define SCHECK( ARG )                                     \
   do {                                                  \
      const bool result = ARG;                           \
      if( ! result ) {                                   \
         ::Error( "VjetTreeMaker", "Failed to execute: \"%s\"", \
                  #ARG );                                \
         return 1;                                       \
      }                                                  \
   } while( false )

#include <AnaAlgorithm/AnaAlgorithm.h>
#include <VjetTreeMaker/Cutflow.h>

namespace VjTM{

static const double GeV = 0.001;

class ObjectSelection : public EL::AnaAlgorithm, public Cutflow
{
public:
  // this is a standard algorithm constructor
  ObjectSelection (const std::string& name, ISvcLocator* pSvcLocator);

  // these are the functions inherited from Algorithm
  virtual StatusCode initialize () override;
  virtual StatusCode execute () override;
  virtual StatusCode finalize () override;

  std::string in_collection;
  std::string out_collection;

  // object selection thresholds
  // init them in derived classes
  double pTmin;
  double etamax;
  double etamin_emcrack = 1.37;
  double etamax_emcrack = 1.52;

private:
  // Configuration, and any other types of variables go here.
  
};

} // namespace VjTM

#endif
