#ifndef VjetTreeMaker_JetR10_H
#define VjetTreeMaker_JetR10_H

#include <VjetTreeMaker/ObjectSelection.h>
#include <xAODJet/JetContainer.h>

#include <AsgTools/AnaToolHandle.h>
#include <JetCalibTools/IJetCalibrationTool.h>
#include <xAODCore/AuxContainerBase.h>
#include <xAODBase/IParticleHelpers.h>

namespace VjTM{

class JetR10 : public ObjectSelection
{
public:
  // this is a standard algorithm constructor
  JetR10 (const std::string& name, ISvcLocator* pSvcLocator);

  // these are the functions inherited from Algorithm
  virtual StatusCode initialize () override;
  virtual StatusCode execute () override;
  virtual StatusCode finalize () override;

  ToolHandle<IJetCalibrationTool> calibration;

private:
  // Configuration, and any other types of variables go here.
};

} // namespace VjTM

#endif
