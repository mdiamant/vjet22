#ifndef VjetTreeMaker_TreeMaker_H
#define VjetTreeMaker_TreeMaker_H

#include <AnaAlgorithm/AnaAlgorithm.h>
#include <TTree.h>
#include <vector>

#include <xAODEventInfo/EventInfo.h>
#include <xAODMissingET/MissingETContainer.h>
#include <xAODJet/JetContainer.h>
#include <xAODEgamma/ElectronContainer.h>
#include <xAODEgamma/PhotonContainer.h>
#include <xAODMuon/MuonContainer.h>

namespace VjTM{

class TreeMaker : public EL::AnaAlgorithm
{
public:
  // this is a standard algorithm constructor
  TreeMaker (const std::string& name, ISvcLocator* pSvcLocator);
  ~TreeMaker ();

  // these are the functions inherited from Algorithm
  virtual StatusCode initialize () override;
  virtual StatusCode execute () override;
  virtual StatusCode finalize () override;

  // input containers
  std::string input_electron;
  std::string input_muon;
  std::string input_gamma;
  std::string input_jet;

  // ------- variable to write ------- //
  // mandatory variables
  unsigned int m_runNumber = 0; ///< Run number
  unsigned long long m_eventNumber = 0; ///< Event number

  // optional, depending on input containers provided

  // Jet 4-momentum variables
  std::vector<float> *m_jetEta = nullptr;
  std::vector<float> *m_jetPhi = nullptr;
  std::vector<float> *m_jetPt = nullptr;
  std::vector<float> *m_jetE = nullptr;

  // ------- end of variable to write ------- //

private:
  // Configuration, and any other types of variables go here.
  //float m_cutValue;
  //TTree *m_myTree;
  //TH1 *m_myHist;
};

} // namespace VjTM

#endif
