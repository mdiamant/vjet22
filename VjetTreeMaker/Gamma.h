#ifndef VjetTreeMaker_Gamma_H
#define VjetTreeMaker_Gamma_H

#include <VjetTreeMaker/ObjectSelection.h>
#include <xAODEgamma/PhotonContainer.h>

#include <AsgTools/AnaToolHandle.h>
#include <EgammaAnalysisInterfaces/IEgammaCalibrationAndSmearingTool.h>
#include <xAODCore/AuxContainerBase.h>
#include <xAODBase/IParticleHelpers.h>
#include <EgammaAnalysisInterfaces/IAsgPhotonIsEMSelector.h>
#include <IsolationSelection/IIsolationSelectionTool.h>

namespace VjTM{

class Gamma : public ObjectSelection
{
public:
  // this is a standard algorithm constructor
  Gamma (const std::string& name, ISvcLocator* pSvcLocator);

  // these are the functions inherited from Algorithm
  virtual StatusCode initialize () override;
  virtual StatusCode execute () override;
  virtual StatusCode finalize () override;

  ToolHandle<CP::IEgammaCalibrationAndSmearingTool> calibration;
  ToolHandle<IAsgPhotonIsEMSelector> selector;
  ToolHandle<CP::IIsolationSelectionTool> isolation;

private:
  // Configuration, and any other types of variables go here.
  
};

} // namespace VjTM

#endif
