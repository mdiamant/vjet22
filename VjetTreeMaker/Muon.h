#ifndef VjetTreeMaker_Muon_H
#define VjetTreeMaker_Muon_H

#include <VjetTreeMaker/ObjectSelection.h>
#include <xAODMuon/MuonContainer.h>

#include <AsgTools/AnaToolHandle.h>
#include <MuonAnalysisInterfaces/IMuonCalibrationAndSmearingTool.h>
#include <MuonAnalysisInterfaces/IMuonSelectionTool.h>
#include <xAODCore/AuxContainerBase.h>
#include <xAODBase/IParticleHelpers.h>
#include <IsolationSelection/IIsolationSelectionTool.h>

namespace VjTM{

class Muon : public ObjectSelection
{
public:
  // this is a standard algorithm constructor
  Muon (const std::string& name, ISvcLocator* pSvcLocator);

  // these are the functions inherited from Algorithm
  virtual StatusCode initialize () override;
  virtual StatusCode execute () override;
  virtual StatusCode finalize () override;

  ToolHandle<CP::IMuonCalibrationAndSmearingTool> calibration;
  ToolHandle<CP::IMuonSelectionTool> selector;
  ToolHandle<CP::IIsolationSelectionTool> isolation;

private:
  // Configuration, and any other types of variables go here.
};

} // namespace VjTM
#endif
