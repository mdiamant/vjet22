#ifndef VjetTreeMaker_Cutflow_H
#define VjetTreeMaker_Cutflow_H

#include <string>
#include <TH1.h>

namespace VjTM{

class Cutflow
{
public:
  Cutflow( const std::string& nm );
  ~Cutflow();

  // fill cutflow histograms (call for each cut each event)
  void cutflow_fill( std::string label, double count, double weight );

  // hist to contain the cutflows (call only in Algo::finalize())
  // e.g. ANA_CHECK( book ( cutflow_hist() ) );
  // this creates histograms and save cutflow stored in vectors
  TH1& cutflow_hist();
  TH1& cutflow_hist_weight();


private:

  std::string name;

  // hist
  TH1* hist_count;
  TH1* hist_weight;

  // cutflow
  std::vector<std::string> cutflow_label; // meaning for each cut
  std::vector<double> cutflow_count; // evt count accumulated for each cut
  std::vector<double> cutflow_weight; // evt weight accumulated for each cut

};

} // namespace VjTM

#endif
