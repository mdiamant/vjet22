#ifndef VjetTreeMaker_JetR04_H
#define VjetTreeMaker_JetR04_H

#include <VjetTreeMaker/ObjectSelection.h>
#include <xAODJet/JetContainer.h>

#include <AsgTools/AnaToolHandle.h>
#include <JetCalibTools/IJetCalibrationTool.h>
#include <JetInterface/IJetUpdateJvt.h>
#include <JetAnalysisInterfaces/IJetJvtEfficiency.h>
#include <xAODCore/AuxContainerBase.h>
#include <xAODBase/IParticleHelpers.h>

namespace VjTM{

class JetR04 : public ObjectSelection
{
public:
  // this is a standard algorithm constructor
  JetR04 (const std::string& name, ISvcLocator* pSvcLocator);

  // these are the functions inherited from Algorithm
  virtual StatusCode initialize () override;
  virtual StatusCode execute () override;
  virtual StatusCode finalize () override;

  ToolHandle<IJetCalibrationTool> calibration;
  ToolHandle<IJetUpdateJvt> calibration_jvt; // nothing to config, make it internal
  ToolHandle<CP::IJetJvtEfficiency> apply_jvt;

private:
  // Configuration, and any other types of variables go here.
};

} // namespace VjTM

#endif
