#ifndef VjetTreeMaker_Electron_H
#define VjetTreeMaker_Electron_H

#include <VjetTreeMaker/ObjectSelection.h>
#include <xAODEgamma/ElectronContainer.h>

#include <AsgTools/AnaToolHandle.h>
#include <EgammaAnalysisInterfaces/IEgammaCalibrationAndSmearingTool.h>
// #pragma message "In the process of moving the Interface part under PhysicsAnalysis/Interfaces/EgammaAnalysisInterfaces"

#include <EgammaAnalysisInterfaces/IAsgElectronLikelihoodTool.h>
//// PhysicsAnalysis/Interfaces/EgammaAnalysisInterfaces/EgammaAnalysisInterfaces/IAsgElectronLikelihoodTool.h
#include <xAODCore/AuxContainerBase.h>
#include <xAODBase/IParticleHelpers.h>
#include <IsolationSelection/IIsolationSelectionTool.h>

#include <ElectronEfficiencyCorrection/AsgElectronEfficiencyCorrectionTool.h>


namespace VjTM{

class Electron : public ObjectSelection
{
public:
  // this is a standard algorithm constructor
  Electron (const std::string& name, ISvcLocator* pSvcLocator);

  // these are the functions inherited from Algorithm
  virtual StatusCode initialize () override;
  virtual StatusCode execute () override;
  virtual StatusCode finalize () override;

  ToolHandle<CP::IEgammaCalibrationAndSmearingTool> calibration;
  //ToolHandle<IAsgElectronLikelihoodTool> selector;
  ToolHandle<CP::IIsolationSelectionTool> isolation;
  ToolHandle<AsgElectronEfficiencyCorrectionTool> effcorr_trig;
  ToolHandle<AsgElectronEfficiencyCorrectionTool> effcorr_reco;
  ToolHandle<AsgElectronEfficiencyCorrectionTool> effcorr_id;
  ToolHandle<AsgElectronEfficiencyCorrectionTool> effcorr_iso;

  // sf deco
  static SG::AuxElement::Accessor<float> effsf_trig;
  static SG::AuxElement::Accessor<float> effsf_reco;
  static SG::AuxElement::Accessor<float> effsf_id;
  static SG::AuxElement::Accessor<float> effsf_iso;

private:
  // Configuration, and any other types of variables go here.
  
  std::string idWorkingPoint;
};

} // namespace VjTM

#endif
