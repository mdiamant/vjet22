#ifndef VJETTREEMAKER_VJETTREEMAKER_DICT_H
#define VJETTREEMAKER_VJETTREEMAKER_DICT_H

// This file includes all the header files that you need to create
// dictionaries for.

#include <VjetTreeMaker/Cutflow.h>
#include <VjetTreeMaker/ObjectSelection.h>
#include <VjetTreeMaker/Pileup.h>
#include <VjetTreeMaker/EventSelection.h>
#include <VjetTreeMaker/EventSelection_ZjetR04.h>
#include <VjetTreeMaker/EventSelection_ZjetR10.h>
#include <VjetTreeMaker/TreeMaker.h>

#include <VjetTreeMaker/JetR04.h>
#include <VjetTreeMaker/JetR10.h>
#include <VjetTreeMaker/Gamma.h>
#include <VjetTreeMaker/Electron.h>
#include <VjetTreeMaker/Muon.h>
#include <VjetTreeMaker/MET.h>

#include <VjetTreeMaker/Tree_ZjetR04.h>
#include <VjetTreeMaker/Tree_ZjetR10.h>
#endif
