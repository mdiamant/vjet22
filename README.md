# VjetTreeMaker

Author: Xiaohu SUN (Xiaohu.Sun@cern.ch)
Update: Magda Diamantopoulou

# Introduction

This is a framework to produce flat ntuples for jet in-situ calibration studies using Vjet events (_Z_+jets, &gamma;+jets etc.).
The aim of this framework is to provide ntuple production with a uniform and highly configurable interface,
for various kinds of Vjets events, and for different calibration methods such as Direct Balance (DB)
and Missing Projection Fraction (MPF).
Based on that, the framework is developed with the latest [EventLoop](https://atlassoftwaredocs.web.cern.ch/ABtutorial/) libraries
and accordingly full pythonic interfaces.

An incomplete list of references for the calibration methods:
- https://cds.cern.ch/record/2242314/files/ATL-COM-PHYS-2017-017.pdf
- https://arxiv.org/pdf/1807.09477.pdf
 
An incomplete list of other relevant softwares used now or in the past:
- https://gitlab.cern.ch/atlas-jetetmiss-jesjer/Insitu/MPF/tree/R21
- https://gitlab.cern.ch/atlas-jetetmiss-jesjer/Insitu/InsituBalance

# Setup and compile for release 22.2.59


```
mkdir  /eos/user/m/mdiamant/Code/VjetsCalib_22.2.59/
cd /eos/user/m/mdiamant/Code/VjetsCalib_22.2.59/
mkdir  build run source
cd source
git clone ssh://git@gitlab.cern.ch:7999/mdiamant/vjet22.git


setupATLAS
cd VjetsCalib_22.2.59/build/
asetup 22.2.59,AnalysisBase,here
gedit CMakeLists.txt 

CMakeLists.txt is:

#
# Project configuration for UserAnalysis.
#

# Set the minimum required CMake version:
cmake_minimum_required( VERSION 3.4 FATAL_ERROR )
project( UserAnalysis VERSION 1.0.0 LANGUAGES CXX )
# Find the AnalysisBase project. This is what, amongst other things, pulls
# in the definition of all of the "atlas_" prefixed functions/macros.
find_package( AnalysisBase 22.2 REQUIRED )
# Set up CTest. This makes sure that per-package build log files can be
# created if the user so chooses.
atlas_ctest_setup()

# Set up the GitAnalysisTutorial project. With this CMake will look for "packages"
# in the current repository and all of its submodules, respecting the
# "package_filters.txt" file, and set up the build of those packages.
atlas_project( USE AnalysisBase ${AnalysisBase_VERSION} )

# Set up the runtime environment setup script. This makes sure that the
# project's "setup.sh" script can set up a fully functional runtime environment,
# including all the externals that the project uses.
lcg_generate_env( SH_FILE ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}/env_setup.sh )
install( FILES ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}/env_setup.sh
   DESTINATION . )

# Set up CPack. This call makes sure that an RPM or TGZ file can be created
# from the built project. Used by Panda to send the project to the grid worker
# nodes.
atlas_cpack_setup()


mv CMakeLists.txt ../source
cmake ../source
cmake --build $TestArea 

#Compilation Errors (see ComplilationError file)

source */setup.sh

