#include <AsgMessaging/MessageCheck.h>
#include <VjetTreeMaker/TreeMaker.h>

namespace VjTM{

TreeMaker :: TreeMaker (const std::string& name,
                                  ISvcLocator *pSvcLocator)
    : EL::AnaAlgorithm (name, pSvcLocator)
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  This is also where you
  // declare all properties for your algorithm.  Note that things like
  // resetting statistics variables or booking histograms should
  // rather go into the initialize() function.

  declareProperty("in_electron", input_electron="NULL", "input container name");
  declareProperty("in_muon", input_muon="NULL", "input container name");
  declareProperty("in_gamma", input_gamma="NULL", "input container name");
  declareProperty("in_jet", input_jet="NULL", "input container name");

}



StatusCode TreeMaker :: initialize ()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.

  ANA_CHECK (book (TTree ("vjet", "TTree for jet insitu calibration using Vjet events")));

  // ------- variable to write ------- //
  // mandatory variables
  TTree* mytree = tree ("vjet");
  mytree->Branch ("RunNumber", &m_runNumber);
  mytree->Branch ("EventNumber", &m_eventNumber);

  // optional, depending on input containers provided

  // Jet 4-momentum variables
  if( input_jet != "NULL" ){
    m_jetEta = new std::vector<float>();
    mytree->Branch ("JetEta", &m_jetEta);
    m_jetPhi = new std::vector<float>();
    mytree->Branch ("JetPhi", &m_jetPhi);
    m_jetPt = new std::vector<float>();
    mytree->Branch ("JetPt", &m_jetPt);
    m_jetE = new std::vector<float>();
    mytree->Branch ("JetE", &m_jetE);
  }

  return StatusCode::SUCCESS;
}



StatusCode TreeMaker :: execute ()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.

  const xAOD::EventInfo* ei = nullptr;
  ANA_CHECK (evtStore()->retrieve (ei, "EventInfo"));

  // ------- variable to write ------- //
  // mandatory variables
  m_runNumber = ei->runNumber ();
  m_eventNumber = ei->eventNumber ();


  // optional, depending on input containers provided

  // Jet 4-momentum variables
  if( input_jet != "NULL" ){
    const xAOD::JetContainer* jets = nullptr;
    ANA_CHECK (evtStore()->retrieve (jets, input_jet));
    m_jetEta->clear();
    m_jetPhi->clear();
    m_jetPt->clear();
    m_jetE->clear();
    for (const xAOD::Jet* jet : *jets) {
      m_jetEta->push_back (jet->eta ());
      m_jetPhi->push_back (jet->phi ());
      m_jetPt-> push_back (jet->pt ());
      m_jetE->  push_back (jet->e ());
    }
  }

  // Fill the event into the tree:
  tree ("vjet")->Fill ();

  return StatusCode::SUCCESS;
}



StatusCode TreeMaker :: finalize ()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.
  return StatusCode::SUCCESS;
}

TreeMaker :: ~TreeMaker(){

  delete m_jetEta;
  delete m_jetPhi;
  delete m_jetPt;
  delete m_jetE;

}

} // namespace VjTM
