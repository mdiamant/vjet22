#include <AsgMessaging/MessageCheck.h>
#include <VjetTreeMaker/EventSelection.h>

namespace VjTM{

EventSelection :: EventSelection (const std::string& name,
                                  ISvcLocator *pSvcLocator)
  : EL::AnaAlgorithm (name, pSvcLocator),
  Cutflow( name ),
  grl("IGoodRunsListSelectionTool/grl", this)
  //prw("IPileupReweightingTool/prw", this),
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  This is also where you
  // declare all properties for your algorithm.  Note that things like
  // resetting statistics variables or booking histograms should
  // rather go into the initialize() function.

  declareProperty("process", process, "The process ZeeJet|ZmmJet|GammaJet");
  declareProperty("isdata", isdata, "Dataset is data or MC");
  declareProperty("year", year, "Year of data takeing");
  declareProperty("grl", grl, "GoodRunList tool");
  //declareProperty("prw", prw, "Pileup reweighting tool");
  declareProperty("triglist201516", triglist201516, "Trigger list");
  declareProperty("triglist2017", triglist2017, "Trigger list");
  declareProperty("triglist2018", triglist2018, "Trigger list");
}



StatusCode EventSelection :: initialize ()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.

  if( year == "201516" ) triglist = triglist201516;
  if( year == "2017" ) triglist = triglist2017;
  if( year == "2018" ) triglist = triglist2018;

  ANA_MSG_INFO("Set year: " << year );
  ANA_MSG_INFO("Set process: " << process );
  ANA_MSG_INFO("Set isdata: " << isdata );
  for( auto _trig : triglist )
    ANA_MSG_INFO("Including trigger: " << _trig);

  if(isdata){ ANA_CHECK ( grl.retrieve() );}
  //else { ANA_CHECK( prw.retrieve() );}

  // trigger
  // these trigger tools do not coorperate with ToolHanlder, gotta use the old way with hard codes
  trigconf = new TrigConf::xAODConfigTool("xAODConfigTool");
  ANA_CHECK( trigconf->initialize() );
  ToolHandle< TrigConf::ITrigConfigTool > trigconf_hdl( trigconf );
  trigdec = new Trig::TrigDecisionTool("TrigDecisionTool");
  trigdec->setProperty("ConfigTool", trigconf_hdl );
  trigdec->setProperty("TrigDecisionKey", "xTrigDecision"); // the container name !!!
  trigmatch = new Trig::MatchingTool("trigmatch");
  ANA_CHECK( trigdec->initialize() );
  ANA_CHECK( trigmatch->initialize() );

  return StatusCode::SUCCESS;
}



StatusCode EventSelection :: execute ()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.
  
  // retrieve the eventInfo object from the event store
  eventInfo = nullptr;
  ANA_CHECK (evtStore()->retrieve (eventInfo, "EventInfo"));
  /*
  if( isdata ){ ANA_CHECK (evtStore()->retrieve (eventInfo, "EventInfo")); } // original eventinfo
  else{ ANA_CHECK (evtStore()->retrieve (eventInfo, "mcEventInfo")); } // modified eventinfo
  */
  // only for MC:
  // mcEventInfo added pileup reweighting decoration before object selections and eff corrections
  // eff corrections need pileup tool to provide RandomRunNumber
  // implicitly this was done when the pileup tool is called
  // eventInfo.auxdecor<unsigned int>("RandomRunNumber") = prw.getRandomRunNumber( eventInfo, true );

  return StatusCode::SUCCESS;
}



StatusCode EventSelection :: finalize ()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.

  // if inherit from Cutflow, always does this in finalize()
  // fill cutflow to histogram
  ANA_CHECK (book ( cutflow_hist() )); 
  ANA_CHECK (book ( cutflow_hist_weight() )); 

  return StatusCode::SUCCESS;
}

bool EventSelection :: pass_alldq(){
  if( !isdata ) return true;
  if( !passLAr() ) return false;
  if( !passTile() ) return false;
  if( !passCore() ) return false;
  if( !passBackground() ) return false;
  if( !passSCT() ) return false;
  if( !passVertex() ) return false;
  return true;
}

// data only
bool EventSelection :: passLAr(){
  if( eventInfo->errorState(xAOD::EventInfo::LAr) == xAOD::EventInfo::Error ) return false;
  return true;
}

// data only
bool EventSelection :: passTile(){
  if( eventInfo->errorState(xAOD::EventInfo::Tile) == xAOD::EventInfo::Error ) return false;
  return true;
}

// data only
// incomplete event
bool EventSelection :: passCore(){
  if( eventInfo->isEventFlagBitSet(xAOD::EventInfo::Core, 18) ) return false;
  return true;
}

// data only
bool EventSelection :: passBackground(){
  if( eventInfo->isEventFlagBitSet(xAOD::EventInfo::Background, 20) ) return false;
  return true;
}

// data only
bool EventSelection :: passSCT(){
  if( eventInfo->errorState(xAOD::EventInfo::SCT) == xAOD::EventInfo::Error ) return false;
  return true;
}

// data only
bool EventSelection :: passVertex(){
  // not in use, see pass_primvertex for hard scattering vertex
  // until self-defined vertex to develop
  return true;
}

bool EventSelection :: pass_primvertex(){
  const xAOD::VertexContainer *vertices = nullptr;
  ANA_CHECK (evtStore()->retrieve (vertices, "PrimaryVertices"));
  if( !vertices ) return false;
  if( vertices->size() < 1 ) return false;
  if( vertices->at(0)->nTrackParticles() >= 2 ) return true; // hard-scatter with >=2 tracks
  return false;
}

bool EventSelection :: pass_trig(){
  bool _pass = false;
  for( auto itrig : triglist ){
    _pass = ( _pass || trigdec->isPassed(itrig) );
  }
  return _pass;
}

bool EventSelection :: pass_trigmch( const xAOD::IParticleContainer* parts ){
  if( parts->size() == 0 ) return false;
  std::vector<const xAOD::IParticle *> _parts;
  for( auto ipart: *parts ){
    _parts.push_back( ipart );
  }
  for( auto itrig : triglist ){
    if( trigmatch->match(_parts, itrig, trigdR, false) ){ // false = not redo trigger
	  return true;
	}
  }
  return false;
}

// calculate invariant mass of [0] and [1] of the container
double EventSelection :: m01( xAOD::IParticleContainer& coll ){
  return (coll[0]->p4() + coll[1]->p4()).M()*GeV;
}

} // namespace VjTM
