#include <AsgMessaging/MessageCheck.h>
#include <VjetTreeMaker/EventSelection_ZjetR04.h>

namespace VjTM{

EventSelection_ZjetR04 :: EventSelection_ZjetR04 (const std::string& name,
                                  ISvcLocator *pSvcLocator)
    : EventSelection (name, pSvcLocator)
{
  declareProperty("elec_collection", elec_collection, "The electron collection name");
  declareProperty("muon_collection", muon_collection, "The muon collection name");
  declareProperty("jetR04_collection", jetR04_collection, "The jetR04 collection name");
  declareProperty("met_collection", met_collection, "The MET collection name");
}



StatusCode EventSelection_ZjetR04 :: initialize ()
{
  EventSelection::initialize();

  ANA_CHECK (book (TTree ("vjet", "TTree for jet insitu calibration using Vjet events")));
  initialize_tree( tree("vjet") );

//  // config overlap removal tool
//  // inputLabel="" do not use decorator to find input objects, instead, apply to all obj
//  ORUtils::ORFlags or_flags("ORTool_jetR10", "", or_outputLabel);
//  // use "standard" setting, i.e. no need to set anything in flags
//  or_flags.outputPassValue = or_outputPassValue;
//  or_flags.doElectrons = isZeeJet();
//  or_flags.doMuons = isZmmJet();
//  or_flags.doJets = true;
//  or_flags.doTaus = false;
//  or_flags.doPhotons = false;
//  or_flags.doFatJets = false;
//  ANA_CHECK( ORUtils::recommendedTools( or_flags, or_toolbox ) );
//  ANA_CHECK( or_toolbox.initialize() );
//
//  ANA_CHECK (book (TH1F ("overlapRM_e", "overlapRM_e", 2, 0, 2)));
//  ANA_CHECK (book (TH1F ("overlapRM_m", "overlapRM_m", 2, 0, 2)));
//  ANA_CHECK (book (TH1F ("overlapRM_j", "overlapRM_j", 2, 0, 2)));
//  hist("overlapRM_e")->GetXaxis()->SetBinLabel(1,"before overlap rm");
//  hist("overlapRM_e")->GetXaxis()->SetBinLabel(2,"after overlap rm");
//  hist("overlapRM_m")->GetXaxis()->SetBinLabel(1,"before overlap rm");
//  hist("overlapRM_m")->GetXaxis()->SetBinLabel(2,"after overlap rm");
//  hist("overlapRM_j")->GetXaxis()->SetBinLabel(1,"before overlap rm");
//  hist("overlapRM_j")->GetXaxis()->SetBinLabel(2,"after overlap rm");

  return StatusCode::SUCCESS;
}



StatusCode EventSelection_ZjetR04 :: execute ()
{
  EventSelection::execute();
  initialize_leaves();
  
  xAOD::ElectronContainer* electrons = nullptr;
  if( isZeeJet() ) ANA_CHECK (evtStore()->retrieve (electrons, elec_collection));

  xAOD::MuonContainer* muons = nullptr;
  if( isZmmJet() ) ANA_CHECK (evtStore()->retrieve (muons, muon_collection));

  xAOD::JetContainer* jets = nullptr;
  ANA_CHECK (evtStore()->retrieve (jets, jetR04_collection));

  xAOD::MissingETContainer* mets = nullptr;
  ANA_CHECK (evtStore()->retrieve (mets, met_collection));

  // overlap removal (house made)
  // MET has its own and is not affected by overlap removal
  if( isZeeJet() ){
	or_removal( electrons, jets, 0.35 );
  }
  if( isZmmJet() ){
	or_removal( muons, jets, 0.35 );
  }

//  // overlap removal
//  // MET has its own and is not affected by overlap removal
//  // calling ATLAS "standard" overlap removal
//  // (electrons, muons, jets, taus, photons, fatJets)
//  //
//  // object counting before overlap removal
//  if(electrons) hist("overlapRM_e")->Fill("before overlap rm",(double)electrons->size());
//  if(muons) hist("overlapRM_m")->Fill("before overlap rm",(double)muons->size());
//  if(jets) hist("overlapRM_j")->Fill("before overlap rm",(double)jets->size());
//  //
//  if(electrons) ANA_MSG_DEBUG("OR_flag electron_n: " << electrons->size() );
//  if(muons)     ANA_MSG_DEBUG("OR_flag muon_n: " << muons->size() );
//  if(jets)   ANA_MSG_DEBUG("OR_flag jetsR04_n: " << jets->size() );
//  ANA_CHECK( or_toolbox.masterTool->removeOverlaps( electrons, muons, jets, nullptr, nullptr, nullptr ) );
//  // the above only decorate, now actuall remove them
//  or_clean( electrons, or_outputLabel, or_outputPassValue);
//  or_clean( muons, or_outputLabel, or_outputPassValue);
//  or_clean( jets, or_outputLabel, or_outputPassValue);
//  ANA_MSG_DEBUG("standard OR done");
//  if(electrons) ANA_MSG_DEBUG("OR_rm electron_n: " << electrons->size() );
//  if(muons)     ANA_MSG_DEBUG("OR_rm muon_n: " << muons->size() );
//  if(jets)   ANA_MSG_DEBUG("OR_rm jetsR04_n: " << jets->size() );
//  // object counting after overlap removal
//  if(electrons) hist("overlapRM_e")->Fill("after overlap rm",(double)electrons->size());
//  if(muons) hist("overlapRM_m")->Fill("after overlap rm",(double)muons->size());
//  if(jets) hist("overlapRM_j")->Fill("after overlap rm",(double)jets->size());

  // sort containers by pT
  if( isZeeJet() ) electrons->sort( descending_pt );
  if( isZmmJet() ) muons->sort( descending_pt );
  if(jets)   jets->sort( descending_pt );
  ANA_MSG_DEBUG("pt-order sorting done");

  // weight
  weight = 1;
  if( !isdata ){
    weight_mc = eventInfo->mcEventWeight(0);
	// move prw to a standalone algo before any obj evt selections
    //weight_pileup =  prw->getCombinedWeight(*eventInfo);
	weight_pileup = eventInfo->auxdecor<float>("PileupWeight");
    weight = weight * weight_mc * weight_pileup;
  }
  // SF of leptons: see below in twolep

  /////////////////////
  // event selection //
  /////////////////////

  // no cut
  pass_init = 1;
  pass_all = pass_init;
  if( pass_init ) cutflow_fill("init", 1, weight);

  // good run list
  pass_grl = 1;
  if( isdata ) pass_grl = (grl->passRunLB(*eventInfo));
  pass_all &= pass_grl;
  if( pass_all ) cutflow_fill("grl",1,weight);

  // triggers
  pass_trigger = pass_trig();
  pass_all &= pass_trigger;
  if( pass_all ) cutflow_fill("trigger", 1, weight);

  // primary vertex
  pass_primary_vertex = pass_primvertex();
  pass_all &= pass_primary_vertex;
  if( pass_all ) cutflow_fill("primary vertex", 1, weight);

  // LarError, TileError, incomplete events revoved in data
  pass_dq = pass_alldq();
  pass_all &= pass_dq;
  if( pass_all ) cutflow_fill("data quality", 1, weight);

  // event cleaning with jet
  // https://twiki.cern.ch/twiki/bin/view/AtlasProtected/HowToCleanJetsR21
  // "If you use the standard loose cleaning working point, you are advised to simply veto events for which the eventClean flag is 0."
  static SG::AuxElement::ConstAccessor<char> eventClean_LooseBadJet("DFCommonJets_eventClean_LooseBad");
  // alternatively eventInfo->auxdata< char >("DFCommonJets_eventClean_LooseBad")
  if( eventClean_LooseBadJet(*eventInfo) )  pass_jetclean = 1;
  pass_all &= pass_jetclean;
  if( pass_all ) cutflow_fill("jet clean", 1, weight);

  // two leptons
  if( isZeeJet() ){
    pass_twolepton = (2==electrons->size());
  }
  if( pass_twolepton ){
	if( !isdata ){
	  weight_lepeff_trig = (Electron :: effsf_trig(*(electrons->at(0))))
	                     * (Electron :: effsf_trig(*(electrons->at(1))));
	  weight_lepeff_reco = (Electron :: effsf_reco(*(electrons->at(0))))
	                     * (Electron :: effsf_reco(*(electrons->at(1))));
	  weight_lepeff_id = (Electron :: effsf_id(*(electrons->at(0))))
	                   * (Electron :: effsf_id(*(electrons->at(1))));
	  weight_lepeff_iso = (Electron :: effsf_iso(*(electrons->at(0))))
	                    * (Electron :: effsf_iso(*(electrons->at(1))));
      weight = ( weight * weight_lepeff_trig * weight_lepeff_reco * weight_lepeff_id * weight_lepeff_iso);
	}
  }
  if( isZmmJet() ){
    pass_twolepton = (2==muons->size());
  }
  pass_all &= pass_twolepton;
  if( pass_all ) cutflow_fill("two lepton", 1, weight);

  // trigger matching
  if( pass_twolepton ){
    if( isZeeJet() ) pass_trigmatch = pass_trigmch( electrons );
    if( isZmmJet() ) pass_trigmatch = pass_trigmch( muons );
  }
  pass_all &= pass_trigmatch;
  if( pass_all ) cutflow_fill("trigger match", 1, weight);

  // opposite charge
  if( pass_twolepton ){
    double _llcharge = 0;
	if( isZeeJet() ) _llcharge = (electrons->at(0)->charge()) * (electrons->at(1)->charge());
	if( isZmmJet() ) _llcharge = (muons->at(0)->charge()) * (muons->at(1)->charge());
	pass_llcharge = (_llcharge<0);
	pass_all &= pass_llcharge;
	if( pass_all ) cutflow_fill("opposite charge", 1, weight);
  }

  // mll window
  if( pass_twolepton ){
    double _mll = 0;
	if( isZeeJet() ) _mll = m01( *electrons );
	if( isZmmJet() ) _mll = m01( *muons );
	if( _mll > 66 && _mll < 116 ) pass_mll = 1;
  }
  pass_all &= pass_mll;
  if( pass_all ) cutflow_fill("mll", 1, weight);

  // njet>=1
  if( jets->size() > 0 ){
    pass_onejet = 1;
  }
  pass_all &= pass_onejet;
  if( pass_all ) cutflow_fill("one+ jet", 1, weight);

  // leading jet pT
  if( jets->size() > 0 ){
    if( jets->at(0)->pt()*GeV > 10 ) pass_j0pt = 1;
  }
  pass_all &= pass_j0pt;
  if( pass_all ) cutflow_fill("leading jet pT", 1, weight);

  // leading jet eta
  if( jets->size() > 0 ){
    if( std::abs(jets->at(0)->eta()) < 0.8 ) pass_j0eta = 1;
  }
  pass_all &= pass_j0eta;
  if( pass_all ) cutflow_fill("leading jet eta", 1, weight);

  // dphi jet and Z
  if( jets->size() > 0 && pass_twolepton ){
    double _dphi = 0;
	TLorentzVector _j = jets->at(0)->p4();
	TLorentzVector _z;
	if( isZeeJet() ) _z = electrons->at(0)->p4() + electrons->at(1)->p4();
	if( isZmmJet() ) _z = muons->at(0)->p4() + muons->at(1)->p4();
	_dphi = _j.DeltaPhi( _z ) ;
	if( std::abs(_dphi) > (TMath::Pi()-0.25) ) pass_dphi_jZ = 1;
  }
  pass_all &= pass_dphi_jZ;
  if( pass_all ) cutflow_fill("dPhi(j0, Z)", 1, weight);

  // subleading jet pT
  if( jets->size() > 1 && pass_twolepton ){
	TLorentzVector _z;
	if( isZeeJet() ) _z = electrons->at(0)->p4() + electrons->at(1)->p4();
	if( isZmmJet() ) _z = muons->at(0)->p4() + muons->at(1)->p4();
	double _ptcut = _z.Pt() * 0.1 * GeV;
	if( ( jets->at(1)->pt()*GeV ) < ( (15 > _ptcut) ? 15 : _ptcut ) ) pass_j1pt = 1;
	//double _ptcut = _z.Pt() * 0.3 * GeV;
	//if( ( jets->at(1)->pt()*GeV ) < ( (12 > _ptcut) ? 12 : _ptcut ) ) pass_j1pt = 1;
  }
  if( jets->size() == 1 ) pass_j1pt = 1;
  pass_all &= pass_j1pt;
  if( pass_all ) cutflow_fill("subleading jet pT", 1, weight);

  /////////////////////
  //  tree filling   //
  /////////////////////

  //
  runnumber = eventInfo->runNumber ();
  eventnumber = eventInfo->eventNumber ();

  //
  TLorentzVector* lep0=nullptr, * lep1=nullptr, * z=nullptr, * j0=nullptr, * j1=nullptr;

  if( isZeeJet() ){
    if( electrons->size()>0 ){
	  lep0 = new TLorentzVector( electrons->at(0)->p4() );
	}
	if( electrons->size()>1 ){
	  lep1 = new TLorentzVector( electrons->at(1)->p4() );
	}
  }

  if( isZmmJet() ){
    if( muons->size()>0 ){
	  lep0 = new TLorentzVector( muons->at(0)->p4() );
	}
	if( muons->size()>1 ){
	  lep1 = new TLorentzVector( muons->at(1)->p4() );
	}
  }

  if( lep0 != nullptr && lep1 != nullptr ){
    z = new TLorentzVector( (*lep0+*lep1) );
  }

  if( jets->size()>0 ){
    j0 = new TLorentzVector( jets->at(0)->p4() );
  }
  if( jets->size()>1 ){
    j1 = new TLorentzVector( jets->at(1)->p4() );
  }

  if( lep0 ){
    lep0_pt = lep0->Pt()*GeV;
	lep0_eta = lep0->Eta();
	lep0_phi = lep0->Phi();
	lep0_e = lep0->E()*GeV;
  }

  if( lep1 ){
    lep1_pt = lep1->Pt()*GeV;
	lep1_eta = lep1->Eta();
	lep1_phi = lep1->Phi();
	lep1_e = lep1->E()*GeV;
  }

  if( z ){
    z_pt = z->Pt()*GeV;
	z_eta = z->Eta();
	z_phi = z->Phi();
	z_e = z->E()*GeV;
	z_m = z->M()*GeV;
  }

  jet_n = jets->size();

  if( j0 ){
    jet0_pt = j0->Pt()*GeV;
	jet0_eta = j0->Eta();
	jet0_phi = j0->Phi();
	jet0_e = j0->E()*GeV;
  }

  if( j1 ){
    jet1_pt = j1->Pt()*GeV;
	jet1_eta = j1->Eta();
	jet1_phi = j1->Phi();
	jet1_e = j1->E()*GeV;
  }

  if( z && j0 ){
    dphi_j0_z = j0->DeltaPhi(*z);
  }

  xAOD::MissingET * refmet = (*mets)["TST"];
  met_x = refmet->mpx()*GeV;
  met_y = refmet->mpy()*GeV;
  met_phi = refmet->phi();
  met = refmet->met()*GeV;

  if( z && j0 ){

	TVector3 _ref_2d( z->Px()*GeV, z->Py()*GeV, 0 );
	auto _ref_2d_unit = _ref_2d.Unit();

	// DB
	TVector3 _j_2d( j0->Px()*GeV, j0->Py()*GeV, 0 );
    resp_db = j0->Pt()*GeV / std::abs( _j_2d.Dot(_ref_2d) / _j_2d.Mag() );

	// MPF
	TVector3 _met_2d( met_x, met_y, 0 );
	resp_mpf = 1 + (_met_2d.Dot(_ref_2d_unit)) / (z->Et()*GeV);
  }

  tree ("vjet")->Fill ();

  delete lep0;
  delete lep1;
  delete z;
  delete j0;
  delete j1;
  return StatusCode::SUCCESS;
}



StatusCode EventSelection_ZjetR04 :: finalize ()
{
  EventSelection::finalize();

  return StatusCode::SUCCESS;
}

} // namespace VjTM
