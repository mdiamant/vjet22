#include <AsgMessaging/MessageCheck.h>
#include <VjetTreeMaker/Cutflow.h>


namespace VjTM{

Cutflow :: Cutflow( const std::string& nm ){

  name = nm;
  hist_count = nullptr;
  hist_weight = nullptr;

}

Cutflow :: ~Cutflow(){

  // hist_count and hist_weight are managed by AthAlgorithm
  // do NOT delete them

}

TH1& Cutflow :: cutflow_hist(){

  unsigned int _size = cutflow_label.size();
  const char* _nm = Form("%s_cutflow", name.c_str() );
  hist_count = new TH1F( _nm, _nm,  _size, 0, _size );
  
  for( unsigned int _i = 1; _i <= _size; ++_i ){
	hist_count->SetBinContent( _i, cutflow_count[_i-1] );
	hist_count->GetXaxis()->SetBinLabel( _i, cutflow_label[_i-1].c_str() );
  }

  return *hist_count;
}

TH1& Cutflow :: cutflow_hist_weight(){

  unsigned int _size = cutflow_label.size();
  const char* _nm = Form("%s_cutflow_weight",name.c_str());
  hist_weight = new TH1F( _nm, _nm,  _size, 0, _size );
  
  for( unsigned int _i = 1; _i <= _size; ++_i ){
	hist_weight->SetBinContent( _i, cutflow_weight[_i-1] );
	hist_weight->GetXaxis()->SetBinLabel( _i, cutflow_label[_i-1].c_str() );
  }

  return *hist_weight;
}

void Cutflow :: cutflow_fill( std::string label, double count, double weight ){

  // first time filling an empty cutflow
  if( cutflow_label.size() == 0 ){
    cutflow_label.push_back(label);
	cutflow_count.push_back(count);
	cutflow_weight.push_back(weight);
    return;
  }

  // filling unempty cutflow
  unsigned int _i = 0;
  for( _i=0; _i < cutflow_label.size(); ++_i ){

    std::string _lbl = cutflow_label[_i];

	// existing slot for a cut
	if( _lbl == label ){
	  cutflow_count[_i] = cutflow_count[_i] + count;
	  cutflow_weight[_i] = cutflow_weight[_i] + weight;
	  return;
	}
  }

  // add a slot for a new cut
  if( _i == cutflow_label.size() ){
    cutflow_label.push_back(label);
    cutflow_count.push_back(count);
    cutflow_weight.push_back(weight);  
  }

  return;
}

} // namespace VjTM
