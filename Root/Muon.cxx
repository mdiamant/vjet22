#include <AsgMessaging/MessageCheck.h>
#include <VjetTreeMaker/Muon.h>

namespace VjTM{

Muon :: Muon (const std::string& name,
                                  ISvcLocator *pSvcLocator)
    : ObjectSelection (name, pSvcLocator),
	  calibration("MuonCalibrationAndSmearingTool/calibration", this),
	  selector("MuonSelectionTool/selector", this),
	  isolation("IsolationSelectionTool/isolation", this)
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  This is also where you
  // declare all properties for your algorithm.  Note that things like
  // resetting statistics variables or booking histograms should
  // rather go into the initialize() function.

  declareProperty("calibration", calibration, "muon calibration tool");
  declareProperty("selector", selector, "muon selector tool");
  declareProperty("isolation", isolation, "muon isolation tool");
}



StatusCode Muon :: initialize ()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.

  ANA_CHECK ( calibration.retrieve() );
  ANA_CHECK ( selector.retrieve() );
  ANA_CHECK ( isolation.retrieve() );

  // see codes in execute()
  ANA_MSG_INFO ( "Muon clearning is not added. The recommendation is to check its impact!" );

  ANA_CHECK (book (TH1F ("m_pt_cut0_init", "m_pt_cut0_init", 1000, 0, 1000)));
  ANA_CHECK (book (TH1F ("m_pt_cut1_ideta",   "m_pt_cut1_ideta", 1000, 0, 1000)));
  ANA_CHECK (book (TH1F ("m_pt_cut2_iso",  "m_pt_cut2_iso", 1000, 0, 1000)));
  ANA_CHECK (book (TH1F ("m_pt_cut3_pt",  "m_pt_cut3_pt", 1000, 0, 1000)));

  ANA_CHECK (book (TH1F ("m_eta_cut0_init", "m_eta_cut0_init", 1000, -5, 5)));
  ANA_CHECK (book (TH1F ("m_eta_cut1_ideta",   "m_eta_cut1_ideta", 1000, -5, 5)));
  ANA_CHECK (book (TH1F ("m_eta_cut2_iso",  "m_eta_cut2_iso", 1000, -5, 5)));
  ANA_CHECK (book (TH1F ("m_eta_cut3_pt",  "m_eta_cut3_pt", 1000, -5, 5)));
  
  return StatusCode::SUCCESS;
}



StatusCode Muon :: execute ()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.

  const xAOD::MuonContainer* in_muons = nullptr;
  ANA_CHECK (evtStore()->retrieve (in_muons, in_collection));

  auto out_muons = std::make_unique<xAOD::MuonContainer>();
  auto out_muons_aux = std::make_unique<xAOD::AuxContainerBase>();
  out_muons->setStore( out_muons_aux.get() );

  for ( auto *imuon : *in_muons ) {
    xAOD::Muon * muon = nullptr;

	// calibration
	// TODO year dependence
	// https://twiki.cern.ch/twiki/bin/view/AtlasProtected/MuonMomentumCorrectionsSubgroup#How_to_handle_Monte_Carlo
    calibration->correctedCopy(*imuon,muon);

    // selection
	bool pass = true;

    // no cut
	cutflow_fill("init",1,1);
	hist( "m_pt_cut0_init" )->Fill(muon->pt()*GeV,1);
	hist( "m_eta_cut0_init" )->Fill(muon->eta(),1);

    // id and eta
    pass &= selector->accept( *muon ); // etamax and id is set externally in joboption
	if(pass){
	  cutflow_fill("id eta",1,1);
	  hist( "m_pt_cut1_ideta" )->Fill(muon->pt()*GeV,1);
	  hist( "m_eta_cut1_ideta" )->Fill(muon->eta(),1);
	}

	// MCP recommends to check the impact of this
	// https://twiki.cern.ch/twiki/bin/view/Atlas/MuonSelectionToolR21
	// is_selector = is_selector && ( ! selector->isBadMuon( *muon ) );

    // iso
    pass &= isolation->accept( *muon ); // set in joboption
	if(pass){
	  cutflow_fill("iso",1,1);
	  hist( "m_pt_cut2_iso" )->Fill(muon->pt()*GeV,1);
	  hist( "m_eta_cut2_iso" )->Fill(muon->eta(),1);
	}

    // pt
	pass &= ( muon->pt()*GeV > pTmin ); // pTmin set in joboption
	if(pass){
	  cutflow_fill("pT",1,1);
	  hist( "m_pt_cut3_pt" )->Fill(muon->pt()*GeV,1);
	  hist( "m_eta_cut3_pt" )->Fill(muon->eta(),1);
	}

    if( pass ){
	  auto muon_sel = new xAOD::Muon();
	  out_muons->push_back( muon_sel );
	  *muon_sel = *muon;
	  xAOD::setOriginalObjectLink( *imuon, *muon_sel ); // to help metMap to find the originals
      // just in case this was not done in calibration()
	}

    delete muon;
  }

  ANA_CHECK (evtStore()->record (out_muons.release(), out_collection));
  ANA_CHECK (evtStore()->record (out_muons_aux.release(), Form("%sAux.", out_collection.c_str()) )); // "Aux." !!!

  return StatusCode::SUCCESS;
}



StatusCode Muon :: finalize ()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.

  // must have it to store cutflow
  ObjectSelection :: finalize();

  return StatusCode::SUCCESS;
}

} // namespace VjTM
