#include <AsgMessaging/MessageCheck.h>
#include <VjetTreeMaker/Pileup.h>

namespace VjTM{

Pileup :: Pileup (const std::string& name,
                                  ISvcLocator *pSvcLocator)
    : EL::AnaAlgorithm (name, pSvcLocator),
	  prw("IPileupReweightingTool/prw", this)
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  This is also where you
  // declare all properties for your algorithm.  Note that things like
  // resetting statistics variables or booking histograms should
  // rather go into the initialize() function.

  declareProperty("prw", prw, "Pileup reweighting tool");
}



StatusCode Pileup :: initialize ()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.

  ANA_CHECK( prw.retrieve() );

  return StatusCode::SUCCESS;
}



StatusCode Pileup :: execute ()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.

  const xAOD::EventInfo * eventInfo = nullptr;
  ANA_CHECK (evtStore()->retrieve (eventInfo, "EventInfo"));

  prw->apply( *eventInfo );
  eventInfo->auxdecor<float>("PileupWeight"); // this returns weight

/*
  auto mcEventinfo = new xAOD::EventInfo();
  auto mcEventinfoAux = new xAOD::EventInfoAuxContainer();
  mcEventinfo->setStore( mcEventinfoAux );

  *mcEventinfo = *eventInfo;

  prw->apply( *mcEventinfo );
  mcEventinfo->auxdecor<float>("PileupWeight"); // this returns weight

  ANA_CHECK (evtStore()->record (mcEventinfo, "mcEventInfo"));
  ANA_CHECK (evtStore()->record (mcEventinfoAux, "mcEventInfoAux." )); // "Aux." !!!
*/

  return StatusCode::SUCCESS;
}



StatusCode Pileup :: finalize ()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.

  return StatusCode::SUCCESS;
}

} // namespace VjTM
