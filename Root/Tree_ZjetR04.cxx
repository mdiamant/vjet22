#include <AsgMessaging/MessageCheck.h>
#include <VjetTreeMaker/Tree_ZjetR04.h>

namespace VjTM{

void Tree_ZjetR04 :: initialize_tree( TTree* tr ){

  // connect to the external tree
  thetree = tr;

  // connect to branches

  // event basic info
  thetree->Branch("runnumber", &runnumber);
  thetree->Branch("eventnumber", &eventnumber);

  // weights
  thetree->Branch("weight", &weight);
  //double weight_ electron muon SF etc.
  thetree->Branch("weight_mc", &weight_mc);
  thetree->Branch("weight_pileup", &weight_pileup);
  thetree->Branch("weight_lepeff_trig", &weight_lepeff_trig);
  thetree->Branch("weight_lepeff_reco", &weight_lepeff_reco);
  thetree->Branch("weight_lepeff_id", &weight_lepeff_id);
  thetree->Branch("weight_lepeff_iso", &weight_lepeff_iso);

  // flag of pass event selections
  thetree->Branch("pass_all", &pass_all);
  thetree->Branch("pass_init", &pass_init);
  thetree->Branch("pass_grl", &pass_grl);
  thetree->Branch("pass_trigger", &pass_trigger);
  thetree->Branch("pass_trigmatch", &pass_trigmatch);
  thetree->Branch("pass_primary_vertex", &pass_primary_vertex);
  thetree->Branch("pass_dq", &pass_dq);
  thetree->Branch("pass_jetclean", &pass_jetclean);
  thetree->Branch("pass_twolepton", &pass_twolepton);
  thetree->Branch("pass_llcharge", &pass_llcharge);
  thetree->Branch("pass_mll", &pass_mll);
  thetree->Branch("pass_onejet", &pass_onejet);
  thetree->Branch("pass_j0pt", &pass_j0pt);
  thetree->Branch("pass_j0eta", &pass_j0eta);
  thetree->Branch("pass_dphi_jZ", &pass_dphi_jZ);
  thetree->Branch("pass_j1pt", &pass_j1pt);

  // variables
  thetree->Branch("lep0_pt", &lep0_pt);
  thetree->Branch("lep0_eta", &lep0_eta);
  thetree->Branch("lep0_phi", &lep0_phi);
  thetree->Branch("lep0_e", &lep0_e);
  
  thetree->Branch("lep1_pt", &lep1_pt);
  thetree->Branch("lep1_eta", &lep1_eta);
  thetree->Branch("lep1_phi", &lep1_phi);
  thetree->Branch("lep1_e", &lep1_e);
 
  thetree->Branch("z_pt", &z_pt);
  thetree->Branch("z_eta", &z_eta);
  thetree->Branch("z_phi", &z_phi);
  thetree->Branch("z_e", &z_e);
  thetree->Branch("z_m", &z_m);

  thetree->Branch("jet_n", &jet_n);

  thetree->Branch("jet0_pt", &jet0_pt);
  thetree->Branch("jet0_eta", &jet0_eta);
  thetree->Branch("jet0_phi", &jet0_phi);
  thetree->Branch("jet0_e", &jet0_e);

  thetree->Branch("jet1_pt", &jet1_pt);
  thetree->Branch("jet1_eta", &jet1_eta);
  thetree->Branch("jet1_phi", &jet1_phi);
  thetree->Branch("jet1_e", &jet1_e);

  thetree->Branch("dphi_j0_z", &dphi_j0_z);

  thetree->Branch("met_x", &met_x);
  thetree->Branch("met_y", &met_y);
  thetree->Branch("met_phi", &met_phi);
  thetree->Branch("met", &met);

  thetree->Branch("resp_db", &resp_db);
  thetree->Branch("resp_mpf", &resp_mpf);

}

void Tree_ZjetR04 :: initialize_leaves(){

  // this should be called before filling any value to variables
  // such as the begining of execute() of the event selection

  // event basic info
  runnumber = 0; ///< Run number
  eventnumber = 0; ///< Event number

  // weights
  weight = 1;
  //double weight_ electron muon SF etc.
  weight_mc = 1; // matrix element
  weight_pileup = 1;
  weight_lepeff_trig = 1;
  weight_lepeff_reco = 1;
  weight_lepeff_id = 1;
  weight_lepeff_iso = 1;
  
  // flag of pass event selections
  pass_all = 0;
  pass_init = 0;
  pass_grl = 0;
  pass_trigger = 0;
  pass_trigmatch = 0;
  pass_primary_vertex = 0;
  pass_dq = 0;
  pass_jetclean = 0;
  pass_twolepton = 0;
  pass_llcharge = 0;
  pass_mll = 0;
  pass_onejet = 0;
  pass_j0pt = 0;
  pass_j0eta = 0;
  pass_dphi_jZ = 0;
  pass_j1pt = 0;

  // variables
  lep0_pt = 0;
  lep0_eta = 0;
  lep0_phi = 0;
  lep0_e = 0;

  lep1_pt = 0;
  lep1_eta = 0;
  lep1_phi = 0;
  lep1_e = 0;

  z_pt = 0;
  z_eta = 0;
  z_phi = 0;
  z_e = 0;
  z_m = 0;

  jet_n = 0;

  jet0_pt = 0;
  jet0_eta = 0;
  jet0_phi = 0;
  jet0_e = 0;

  jet1_pt = 0;
  jet1_eta = 0;
  jet1_phi = 0;
  jet1_e = 0;

  dphi_j0_z = 0;

  met_x = 0;
  met_y = 0;
  met_phi = 0;
  met = 0;

  resp_db = 0;
  resp_mpf = 0;
}

} // namespace VjTM
