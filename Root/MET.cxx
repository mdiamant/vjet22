#include <AsgMessaging/MessageCheck.h>
#include <VjetTreeMaker/MET.h>
#include "METUtilities/METHelpers.h"
#include "METInterface/IMETSystematicsTool.h"
#include "METInterface/IMETSignificance.h"
#include "xAODMissingET/MissingETAuxContainer.h"
#include "METUtilities/METHelpers.h"



namespace VjTM{

MET :: MET (const std::string& name,
                                  ISvcLocator *pSvcLocator)
    : ObjectSelection (name, pSvcLocator),
      calibration("METMaker/calibration", this)
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  This is also where you
  // declare all properties for your algorithm.  Note that things like
  // resetting statistics variables or booking histograms should
  // rather go into the initialize() function.

  declareProperty("calibration", calibration, "MET calibration tool");

  declareProperty("process", process="NULL", "ZeeJet | ZmumuJet | GammaJet");
  declareProperty("jettype", jettype="NULL", "AntiKt4EMTopo etc.");

  declareProperty("in_electron", input_electron="NULL", "input container name");
  declareProperty("in_muon", input_muon="NULL", "input container name");
  declareProperty("in_gamma", input_gamma="NULL", "input container name");
  declareProperty("in_jet", input_jet="NULL", "input container name");

}



StatusCode MET :: initialize ()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.

  ANA_MSG_INFO("Set process: " << process);
  ANA_MSG_INFO("Set jettype: " << jettype);
  ANA_MSG_INFO("Set in_electron: " << input_electron);
  ANA_MSG_INFO("Set in_muon: " << input_muon);
  ANA_MSG_INFO("Set in_gamma: " << input_gamma);
  ANA_MSG_INFO("Set in_jet: " << input_jet);

  // process has to be set
  if( process == "NULL" ){
    ANA_MSG_ERROR("EXIT, no process type is provided for MET calculation");
    ANA_MSG_ERROR("Examples: MET.process='ZeeJet' # or ZmmJet or GammaJet");
    return StatusCode::FAILURE;
  }else{
    ANA_MSG_INFO("Process type is " << process << " for MET calculation.");
  }

  if( (process == "ZeeJet" && input_electron != "NULL" && input_jet != "NULL") 
    ||(process == "ZmmJet" && input_muon != "NULL" && input_jet != "NULL")
    ||(process == "GammaJet" && input_gamma != "NULL" && input_jet != "NULL") ){
  }else{
    ANA_MSG_ERROR("For process " << process << " no sufficient input containers are set");
    ANA_MSG_ERROR("[electron]" << input_electron);
    ANA_MSG_ERROR("[muon]" << input_muon);
    ANA_MSG_ERROR("[gamma]" << input_gamma);
    ANA_MSG_ERROR("[jet]" << input_jet);
    return StatusCode::FAILURE;
  }

  ANA_CHECK ( calibration.retrieve() );

  return StatusCode::SUCCESS;
}



StatusCode MET :: execute ()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.

  // core MET
  const xAOD::MissingETContainer* coreMet  = nullptr;
  std::string coreMetKey = "MET_Core_" + jettype;
  if( jettype == "NULL" ){
    ANA_MSG_ERROR("Jet type use to calc MET is not provided");
    return StatusCode::FAILURE;
  }
  ANA_CHECK( evtStore()->retrieve(coreMet, coreMetKey) ); 

  // retrieve the MET association map
  const xAOD::MissingETAssociationMap* metMap = nullptr;
  std::string metAssocKey = "METAssoc_" + jettype;
  ANA_CHECK( evtStore()->retrieve(metMap, metAssocKey) );

  // before proceeding with every MET calculation
  //metMap->resetObjSelectionFlags();

//HERE!!!!!
  xAOD::MissingETAssociationHelper metHelper(&(*metMap));
  metHelper.resetObjSelectionFlags();

  // create a MET with its aux store for each systematic
  auto newMetContainer = std::make_unique<xAOD::MissingETContainer>();
  auto newMetAuxContainer = std::make_unique<xAOD::MissingETAuxContainer>();
  newMetContainer->setStore(newMetAuxContainer.get());

  // all processes in Vjet have jets
  xAOD::JetContainer* jets = nullptr;
  ANA_CHECK (evtStore()->retrieve (jets, input_jet+"_formet"));
  // special full jet collection only for MET calculation!
  // and make sure "Jvt" is used as the derocation name for jvt, default to call in MET calc

  // VERY IMPORTANT
  // https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/METUtilities
  // 0. definitely before any overlap removal!
  // 1. pass electron, muon, tau or photon after calibration/id/iso/selection
  // 2. jet needs to be the whole jet collection, having calibration and JVT decoration but no selection (not JVT cut!!!)
  //    for this, in class Jet, a jet collection with user_defined_name+"_formet" is saved
  // 3. make sure use SG::VIEW_ELEMENTS in creating containers for MET
  //    this prevents the containers for MET to own/delete the particles in them


  // electron muon or gamma dependes on the process
  //
  // Zee+jet events
  if( process == "ZeeJet" ){
    ConstDataVector<xAOD::ElectronContainer> metElectrons(SG::VIEW_ELEMENTS);
    const xAOD::ElectronContainer* electrons = nullptr;
    ANA_CHECK (evtStore()->retrieve (electrons, input_electron));
    for(const auto& el : *electrons) {
      if(1){ metElectrons.push_back(el); }
    }
    ANA_CHECK( calibration->rebuildMET("RefEle",                   //name of metElectrons in metContainer
               xAOD::Type::Electron,       //telling the rebuilder that this is electron met
               newMetContainer.get(),            //filling this met container
               metElectrons.asDataVector(),//using these metElectrons that accepted our cuts
               metHelper)                     //and this association map edw
             );
  //
  // Zmm+jets events
  }else if( process == "ZmmJet" ){
  // see below
  //
  // Gamma+jets events
  }else if( process == "GammaJet"){
    ConstDataVector<xAOD::PhotonContainer> metGammas(SG::VIEW_ELEMENTS);
    const xAOD::PhotonContainer* gammas = nullptr;
    ANA_CHECK (evtStore()->retrieve (gammas, input_gamma));
    for(const auto& ph : *gammas) {
      if(1){ metGammas.push_back(ph); }
    }
    ANA_CHECK( calibration->rebuildMET("RefPhoton",
               xAOD::Type::Photon,
               newMetContainer.get(),
               metGammas.asDataVector(),
               metHelper) //edw
             );
  }else{
    ANA_MSG_ERROR("Process ["<<process<<"] is not supported");
    return StatusCode::FAILURE;
  }

  // https://twiki.cern.ch/twiki/bin/view/AtlasProtected/EtmissRecommendationsRel21p2
  // MET has its own overlap removal so all jets should be fed into the tool
  
  // always to have muon in met calc, even muons.size()==0
  // to suppress "WARNING Attempted to apply muon Eloss correction, but corresponding MET term does not exist!"
  // which is failing the run!
  // ZmmJet or not, always include muons
  ConstDataVector<xAOD::MuonContainer> metMuons(SG::VIEW_ELEMENTS);
  const xAOD::MuonContainer* muons = nullptr;
  if( process == "ZmmJet" ) ANA_CHECK (evtStore()->retrieve (muons, input_muon));
  if( muons ){
    for(const auto& mu : *muons) {
      if(1){ metMuons.push_back(mu); }
    }
  }
  else{
    muons = new const xAOD::MuonContainer(); // addGhostMuonsToJets
  }
  ANA_CHECK( calibration->rebuildMET("Muons",
             xAOD::Type::Muon,
             newMetContainer.get(),
             metMuons.asDataVector(),
             metHelper) //edw
           );
  // special for muons and jets before handing the jets and muons to METUtility
  met::addGhostMuonsToJets(*muons, *jets);

  //Now time to rebuild jetMet and get the soft term
  //This adds the necessary soft term for both CST and TST
  //these functions create an xAODMissingET object with the given names inside the container
  ANA_CHECK( calibration->rebuildJetMET("RefJet",        //name of jet met
             "SoftClus",      //name of soft cluster term met
             "PVSoftTrk",     //name of soft track term met
             newMetContainer.get(), //adding to this new met container
             jets,       //using this jet collection to calculate jet met
             coreMet,         //core met container
             metHelper,          //with this association map edw
             true            //apply jet jvt cut
             )
  );

  // not having trkMET for the moment
  // METMaker::rebuildTrackMET()

  // TODO syst
  // soft cluster soft tracks need additional syst

  // pflow TST MET to use later
  //calibration->buildMETSum("TST", newMetContainer.get(), MissingETBase::Source::Track); //edw
//calibration->met::buildMETSum("TST", &met, met[softTerm]->source())


  ANA_CHECK (evtStore()->record (newMetContainer.release(), out_collection));
  ANA_CHECK (evtStore()->record (newMetAuxContainer.release(), Form("%sAux.", out_collection.c_str()) )); // "Aux." !!!

  return StatusCode::SUCCESS;
}



StatusCode MET :: finalize ()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.

  // must have it to store cutflow
  ObjectSelection :: finalize();

  return StatusCode::SUCCESS;
}

} // namespace VjTM
