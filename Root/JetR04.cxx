#include <AsgMessaging/MessageCheck.h>
#include <VjetTreeMaker/JetR04.h>

namespace VjTM{

JetR04 :: JetR04 (const std::string& name,
                                  ISvcLocator *pSvcLocator)
    : ObjectSelection (name, pSvcLocator),
	  calibration("JetCalibrationTool/calibration", this),
	  calibration_jvt("JetVertexTaggerTool/calibration_jvt", this),
	  apply_jvt("JetJvtEfficiency/apply_jvt", this)
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  This is also where you
  // declare all properties for your algorithm.  Note that things like
  // resetting statistics variables or booking histograms should
  // rather go into the initialize() function.

  declareProperty("calibration", calibration, "jet calibration tool");
  // must call in this way in control file
  // addPrivateTool( alg_seljet, 'calibration_jvt', 'JetVertexTaggerTool' )
  // JetVertexTaggerTool, instead of JetVertexTaggerTool/UpdateJVT or UpdateJVT or ...
  declareProperty("calibration_jvt", calibration_jvt, "jvt calibration tool");
  declareProperty("apply_jvt", apply_jvt, "jvt application and sf tool");
  declareProperty("pTmin", pTmin, "pT cut");
  declareProperty("etamax", etamax, "eta cut");

}



StatusCode JetR04 :: initialize ()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.

  ANA_CHECK ( calibration.retrieve() );
  ANA_CHECK ( calibration_jvt.retrieve() );
  ANA_CHECK ( apply_jvt.retrieve() );

  ANA_CHECK (book (TH1F ("j_pt_cut0_init", "j_pt_cut0_init", 1000, 0, 1000)));
  ANA_CHECK (book (TH1F ("j_pt_cut1_jvt",   "j_pt_cut1_jvt", 1000, 0, 1000)));
  ANA_CHECK (book (TH1F ("j_pt_cut2_pt",   "j_pt_cut2_pt", 1000, 0, 1000)));
  ANA_CHECK (book (TH1F ("j_pt_cut3_eta",   "j_pt_cut3_eta", 1000, 0, 1000)));

  ANA_CHECK (book (TH1F ("j_eta_cut0_init", "j_eta_cut0_init", 1000, -5, 5)));
  ANA_CHECK (book (TH1F ("j_eta_cut1_jvt",   "j_eta_cut1_jvt", 1000, -5, 5)));
  ANA_CHECK (book (TH1F ("j_eta_cut2_pt",   "j_eta_cut2_pt", 1000, -5, 5)));
  ANA_CHECK (book (TH1F ("j_eta_cut3_eta",   "j_eta_cut3_eta", 1000, -5, 5)));
  
  return StatusCode::SUCCESS;
}



StatusCode JetR04 :: execute ()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.

  const xAOD::JetContainer* in_jets = nullptr;
  ANA_CHECK (evtStore()->retrieve (in_jets, in_collection));

  auto out_jets = std::make_unique<xAOD::JetContainer>();
  auto out_jets_aux = std::make_unique<xAOD::AuxContainerBase>();
  out_jets->setStore( out_jets_aux.get() );

  // this is a full jet collection with out filtering
  // but calibration and jvt decoration is done
  // needed by MET calculation otf!!!
  auto out_jets_formet = std::make_unique<xAOD::JetContainer>();
  auto out_jets_aux_formet = std::make_unique<xAOD::AuxContainerBase>();
  out_jets_formet->setStore( out_jets_aux_formet.get() );

  for ( auto *ijet : *in_jets ) {
    xAOD::Jet * jet = nullptr;


	// calibration
   // calibration->calibratedCopy(*ijet,jet); //HERE
	// pay special attention to these calibration functions
	// not only electrons, including all other objects
	// they all call makePrivateStore() to allow single IParticlar to have private auxdata
	// these will not be attached to or managed by any container to which the object is added to
	// thus if these object are not deleted, there will be huge mem leak !!!
	// action
	// 1. copy these objects to new ones (private auxdata will not be copied)
	// 2. use the copied clean object to push in out going containers
	// 3. delete these calibrated objects that have private auxdata

	// selection
	bool pass = true;

	// no cut
	cutflow_fill("init",1,1);
	hist( "j_pt_cut0_init" )->Fill(jet->pt()*GeV,1);
	hist( "j_eta_cut0_init" )->Fill(jet->eta(),1);

    // jvt recalculation
	// https://twiki.cern.ch/twiki/bin/view/AtlasProtected/PileupJetRecommendations
	// depends on the calibration, recommended to recalculate JVT at analysis level
	double _jvt = calibration_jvt -> updateJvt( *jet );
	jet->auxdata<float>("Jvt") = _jvt;
	// BE SURE TO use "Jvt" which is the default name defined in MET as well
	// MET will read this decoration
	// https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/METUtilities

	// fjvt not used atm

	// jvt cut
	// ATTENTION: jvt sf assumes that the jet container passed to the tool contains signal jets
	//            after Overlap Removal but before applying JVT
	// Please note "passesJvtCut" is applied only on central jets; forward jets will always pass the selection.
	// jetsf->setProperty("MaxPtForJvt",60e3); is set in pythonic control file externally
	//
	//
	// do NOT use this function which has a pT hole (pT<20GeV returns false always)
	// pass &= apply_jvt->passesJvtCut( *jet );
	// take cuts by hand from # https://twiki.cern.ch/twiki/bin/view/AtlasProtected/PileupJetRecommendations
	if( (jet->pt()*GeV<60) && (abs(jet->eta())<2.4) ){
	  // apply jvt cut
	  if( _jvt > 0.5 ) pass &= true;
	  else pass &= false;
	}else{
	  pass &= true;
	}
	if(pass){
	  cutflow_fill("jvt",1,1);
	  hist( "j_pt_cut1_jvt" )->Fill(jet->pt()*GeV,1);
	  hist( "j_eta_cut1_jvt" )->Fill(jet->eta(),1);
	}

	// pT
	pass &= (jet->pt()*GeV > pTmin);
	if(pass){
	  cutflow_fill("pt",1,1);
	  hist( "j_pt_cut2_pt" )->Fill(jet->pt()*GeV,1);
	  hist( "j_eta_cut2_pt" )->Fill(jet->eta(),1);
	}

    // eta
	pass &= (abs(jet->eta()) < etamax );
	if(pass){
	  cutflow_fill("eta",1,1);
	  hist( "j_pt_cut3_eta" )->Fill(jet->pt()*GeV,1);
	  hist( "j_eta_cut3_eta" )->Fill(jet->eta(),1);
	}


	// dR(j,J) in overlap removal
	// pT max cut in event level

	//
	if( pass ){
	  auto jet_sel = new xAOD::Jet();
	  out_jets->push_back( jet_sel );
	  *jet_sel = *jet;
	  xAOD::setOriginalObjectLink( *ijet, *jet_sel ); // to help metMap to find the originals
      // just in case this was not done in calibration()
	}

    // for MET calculation only
	// the whole jet collection with no selection
	auto jet_sel_formet = new xAOD::Jet();
	out_jets_formet->push_back( jet_sel_formet );
	*jet_sel_formet = *jet;
	xAOD::setOriginalObjectLink( *ijet, *jet_sel_formet ); // to help metMap to find the originals
    // just in case this was not done in calibration()

	delete jet;

  }

  ANA_CHECK (evtStore()->record (out_jets.release(), out_collection));
  ANA_CHECK (evtStore()->record (out_jets_aux.release(), Form("%sAux.", out_collection.c_str()) )); // "Aux." !!!

  ANA_CHECK (evtStore()->record (out_jets_formet.release(), out_collection+"_formet"));
  ANA_CHECK (evtStore()->record (out_jets_aux_formet.release(), Form("%sAux.", (out_collection+"_formet").c_str()) )); // "Aux." !!!

  return StatusCode::SUCCESS;
}



StatusCode JetR04 :: finalize ()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.

  // must have it to store cutflow
  ObjectSelection :: finalize();

  return StatusCode::SUCCESS;
}

} // namespace VjTM
