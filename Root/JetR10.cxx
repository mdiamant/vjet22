#include <AsgMessaging/MessageCheck.h>
#include <VjetTreeMaker/JetR10.h>

namespace VjTM{

JetR10 :: JetR10 (const std::string& name,
                                  ISvcLocator *pSvcLocator)
    : ObjectSelection (name, pSvcLocator),
	  calibration("JetCalibrationTool/calibration", this)
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  This is also where you
  // declare all properties for your algorithm.  Note that things like
  // resetting statistics variables or booking histograms should
  // rather go into the initialize() function.

  declareProperty("calibration", calibration, "jet calibration tool");
}



StatusCode JetR10 :: initialize ()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.

  ANA_CHECK ( calibration.retrieve() );

  return StatusCode::SUCCESS;
}



StatusCode JetR10 :: execute ()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.

  const xAOD::JetContainer* in_jets = nullptr;
  ANA_CHECK (evtStore()->retrieve (in_jets, in_collection));

  auto out_jets = std::make_unique<xAOD::JetContainer>();
  auto out_jets_aux = std::make_unique<xAOD::AuxContainerBase>();
  out_jets->setStore( out_jets_aux.get() );

  for ( auto *ijet : *in_jets ) {
    xAOD::Jet * jet = nullptr;

	// calibration
    //calibration->calibratedCopy(*ijet,jet);
	// pay special attention to these calibration functions
	// not only electrons, including all other objects
	// they all call makePrivateStore() to allow single IParticlar to have private auxdata
	// these will not be attached to or managed by any container to which the object is added to
	// thus if these object are not deleted, there will be huge mem leak !!!
	// action
	// 1. copy these objects to new ones (private auxdata will not be copied)
	// 2. use the copied clean object to push in out going containers
	// 3. delete these calibrated objects that have private auxdata


	// selection
    bool pass = true;

    // not cut
	cutflow_fill("init", 1, 1);

    // eta
	pass &= std::abs(jet->eta()) < etamax;
	if(pass) cutflow_fill("eta",1,1);

	// pT
    pass &= (jet->pt()*GeV > pTmin);
	if(pass) cutflow_fill("pT",1,1);

	//
    if( pass ){
	  auto jet_sel = new xAOD::Jet();
	  out_jets->push_back( jet_sel );
	  *jet_sel = *jet;
	  xAOD::setOriginalObjectLink( *ijet, *jet_sel ); // to help metMap to find the originals
      // just in case this was not done in calibration()
	}
	delete jet;
  }

  ANA_CHECK (evtStore()->record (out_jets.release(), out_collection));
  ANA_CHECK (evtStore()->record (out_jets_aux.release(), Form("%sAux.", out_collection.c_str()) )); // "Aux." !!!

  return StatusCode::SUCCESS;
}



StatusCode JetR10 :: finalize ()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.

  // must have it to store cutflow
  ObjectSelection :: finalize();

  return StatusCode::SUCCESS;
}

} // namespace VjTM
