#include <AsgMessaging/MessageCheck.h>
#include <VjetTreeMaker/Electron.h>

namespace VjTM{

SG::AuxElement::Accessor<float> Electron :: effsf_trig("effsf_trig");
SG::AuxElement::Accessor<float> Electron :: effsf_reco("effsf_reco");
SG::AuxElement::Accessor<float> Electron :: effsf_id("effsf_id");
SG::AuxElement::Accessor<float> Electron :: effsf_iso("effsf_iso");

Electron :: Electron (const std::string& name,
                                  ISvcLocator *pSvcLocator)
    : ObjectSelection (name, pSvcLocator),
	  calibration("EgammaCalibrationAndSmearingTool/calibration", this),
	  //selector("AsgElectronLikelihoodTool/selector", this),
    isolation("IsolationSelectionTool/isolation", this),
	  effcorr_trig("AsgElectronEfficiencyCorrectionTool/effcorr_trig",this),
	  effcorr_reco("AsgElectronEfficiencyCorrectionTool/effcorr_reco",this),
	  effcorr_id("AsgElectronEfficiencyCorrectionTool/effcorr_id",this),
	  effcorr_iso("AsgElectronEfficiencyCorrectionTool/effcorr_iso",this)
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  This is also where you
  // declare all properties for your algorithm.  Note that things like
  // resetting statistics variables or booking histograms should
  // rather go into the initialize() function.

  declareProperty("calibration", calibration, "egamma calibration tool");
  //declareProperty("selector", selector, "egamma ID tool");
  declareProperty("effcorr_trig", effcorr_trig, "efficiency correction tool");
  declareProperty("effcorr_reco", effcorr_reco, "efficiency correction tool");
  declareProperty("effcorr_id", effcorr_id, "efficiency correction tool");
  declareProperty("effcorr_iso", effcorr_iso, "efficiency correction tool");
  declareProperty("idWorkingPoint", idWorkingPoint, "DFCommonWP for egamma ID");
  declareProperty("isolation", isolation, "egamma isolation tool");

}



StatusCode Electron :: initialize ()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.

  ANA_CHECK ( calibration.retrieve() );
  //ANA_CHECK ( selector.retrieve() );
  ANA_CHECK ( effcorr_trig.retrieve() );
  ANA_CHECK ( effcorr_reco.retrieve() );
  ANA_CHECK ( effcorr_id.retrieve() );
  ANA_CHECK ( effcorr_iso.retrieve() );
  ANA_CHECK ( isolation.retrieve() );

  idWorkingPoint = ("DFCommon"+idWorkingPoint);
  ANA_MSG_INFO("use DF level electron id: " << idWorkingPoint);

  ANA_CHECK (book (TH1F ("e_pt_cut0_init", "e_pt_cut0_init", 1000, 0, 1000)));
  ANA_CHECK (book (TH1F ("e_pt_cut1_id",   "e_pt_cut1_id", 1000, 0, 1000)));
  ANA_CHECK (book (TH1F ("e_pt_cut2_iso",  "e_pt_cut2_iso", 1000, 0, 1000)));
  ANA_CHECK (book (TH1F ("e_pt_cut3_eta",  "e_pt_cut3_eta", 1000, 0, 1000)));
  ANA_CHECK (book (TH1F ("e_pt_cut4_pt",   "e_pt_cut4_pt", 1000, 0, 1000)));

  ANA_CHECK (book (TH1F ("e_eta_cut0_init", "e_eta_cut0_init", 1000, -5, 5)));
  ANA_CHECK (book (TH1F ("e_eta_cut1_id",   "e_eta_cut1_id", 1000, -5, 5)));
  ANA_CHECK (book (TH1F ("e_eta_cut2_iso",  "e_eta_cut2_iso", 1000, -5, 5)));
  ANA_CHECK (book (TH1F ("e_eta_cut3_eta",  "e_eta_cut3_eta", 1000, -5, 5)));
  ANA_CHECK (book (TH1F ("e_eta_cut4_pt",   "e_eta_cut4_pt", 1000, -5, 5)));

  return StatusCode::SUCCESS;
}



StatusCode Electron :: execute ()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.

  const xAOD::ElectronContainer* in_electrons = nullptr;
  ANA_CHECK (evtStore()->retrieve (in_electrons, in_collection));

  auto out_electrons = std::make_unique<xAOD::ElectronContainer>();
  auto out_electrons_aux = std::make_unique<xAOD::AuxContainerBase>();
  out_electrons->setStore( out_electrons_aux.get() );

  for ( auto *ielectron : *in_electrons ) {
    xAOD::Electron * electron = nullptr;


	// calibration
    calibration->correctedCopy(*ielectron, electron);
	// pay special attention to these calibration functions
	// not only electrons, including all other objects
	// they all call makePrivateStore() to allow single IParticlar to have private auxdata
	// these will not be attached to or managed by any container to which the object is added to
	// thus if these object are not deleted, there will be huge mem leak !!!
	// action
	// 1. copy these objects to new ones (private auxdata will not be copied)
	// 2. use the copied clean object to push in out going containers
	// 3. delete these calibrated objects that have private auxdata

    // selection
	bool pass = true;

    // not cut
	cutflow_fill("init", 1, 1);
	hist( "e_pt_cut0_init" )->Fill(electron->pt()*GeV,1);
	hist( "e_eta_cut0_init" )->Fill(electron->eta(),1);

    // ID
	//is_selector = selector->accept( electron ); // ok to before/after calibration, ID tools use cluster->e() that is not changed by calibration
	// "At the moment, the selector tools for the electron selection need to be setup by hand,
	// or the decorations added during derivation have to be used (see above). The flags stored in the xAODs cannot be used"
    //pass &= selector->accept( electron );
	static SG::AuxElement::ConstAccessor<char> _acc_id(idWorkingPoint);
	pass &= ( _acc_id( *electron ) );
	if(pass){
	  cutflow_fill("id",1,1);
	  hist( "e_pt_cut1_id" )->Fill(electron->pt()*GeV,1);
	  hist( "e_eta_cut1_id" )->Fill(electron->eta(),1);
	}

    // ISO
	// https://twiki.cern.ch/twiki/bin/view/AtlasProtected/RecommendedIsolationWPs
	// iso correction tool is not required for electrons
	// thus call the isolation selector directly
	//pass &= isolation->accept( *electron ); HERE!!!
	if(isolation->accept( *electron )){
	  cutflow_fill("iso",1,1);
	  hist( "e_pt_cut2_iso" )->Fill(electron->pt()*GeV,1);
	  hist( "e_eta_cut2_iso" )->Fill(electron->eta(),1);
	}

    // eta cut and crack
    const xAOD::CaloCluster* eCluster(0);
    if( electron->nCaloClusters() > 0) {eCluster = electron->caloCluster(0);}
	if( eCluster ){
	  double _abseta = std::abs( eCluster->eta() );
	  pass &= ( _abseta < etamax ) && !( _abseta > etamin_emcrack && _abseta < etamax_emcrack );
	}else{
	  pass &= false;
	}
	if(pass){
	  cutflow_fill("eta and crack",1,1);
	  hist( "e_pt_cut3_eta" )->Fill(electron->pt()*GeV,1);
  	  hist( "e_eta_cut3_eta" )->Fill(electron->eta(),1);
	}
	//double _abseta = std::abs(electron->eta());
	//pass &= ( _abseta < etamax ) && !( _abseta > etamin_emcrack && _abseta < etamax_emcrack );
	//if(pass){
	//  cutflow_fill("eta and crack",1,1);
	//  hist( "e_pt_cut3_eta" )->Fill(electron->pt()*GeV,1);
	//  hist( "e_eta_cut3_eta" )->Fill(electron->eta(),1);
	//}

    // pT cut
    pass &= ( electron->pt()*GeV > pTmin );
	if(pass){
	  cutflow_fill("pT",1,1);
	  hist( "e_pt_cut4_pt" )->Fill(electron->pt()*GeV,1);
	  hist( "e_eta_cut4_pt" )->Fill(electron->eta(),1);
	}

    //
    if( pass ){
	  auto electron_sel = new xAOD::Electron();
	  out_electrons->push_back( electron_sel );
	  *electron_sel = *electron;
	  xAOD::setOriginalObjectLink( *ielectron, *electron_sel ); // to help metMap to find the originals
	  // just in case this was not done in calibration()

	  // sf
	  double _sf_trig(1.0), _sf_reco(1.0), _sf_id(1.0), _sf_iso(1.0);
	  // TODO check the return codes of sf tools !
      effcorr_trig->getEfficiencyScaleFactor( *electron_sel, _sf_trig );
	  effcorr_reco->getEfficiencyScaleFactor( *electron_sel, _sf_reco );
	  effcorr_id->getEfficiencyScaleFactor( *electron_sel, _sf_id );
	  effcorr_iso->getEfficiencyScaleFactor( *electron_sel, _sf_iso );
	  effsf_trig(*electron_sel) = _sf_trig;
	  effsf_reco(*electron_sel) = _sf_reco;
	  effsf_id(*electron_sel) = _sf_id;
	  effsf_iso(*electron_sel) = _sf_iso;

	}

	delete electron; // important to clean up calibrated obj which has private auxdata

  }

  ANA_CHECK (evtStore()->record (out_electrons.release(), out_collection));
  ANA_CHECK (evtStore()->record (out_electrons_aux.release(), Form("%sAux.", out_collection.c_str()) )); // "Aux." !!!

  return StatusCode::SUCCESS;
}



StatusCode Electron :: finalize ()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.

  // must have it to store cutflow
  ObjectSelection :: finalize();

  return StatusCode::SUCCESS;
}

} // namespace VjTM
