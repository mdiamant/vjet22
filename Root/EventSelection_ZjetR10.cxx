#include <AsgMessaging/MessageCheck.h>
#include <VjetTreeMaker/EventSelection_ZjetR10.h>

namespace VjTM{

EventSelection_ZjetR10 :: EventSelection_ZjetR10 (const std::string& name,
                                  ISvcLocator *pSvcLocator)
    : EventSelection (name, pSvcLocator)
{
  declareProperty("elec_collection", elec_collection, "The electron collection name");
  declareProperty("muon_collection", muon_collection, "The muon collection name");
  declareProperty("jetR04_collection", jetR04_collection, "The jet R04 collection name");
  declareProperty("jetR10_collection", jetR10_collection, "The jet R10 collection name");
}



StatusCode EventSelection_ZjetR10 :: initialize ()
{
  EventSelection::initialize();

  ANA_CHECK (book (TTree ("vjet", "TTree for jet insitu calibration using Vjet events")));
  initialize_tree( tree("vjet") );

  // config overlap removal tool
  // inputLabel="" do not use decorator to find input objects, instead, apply to all obj
  ORUtils::ORFlags or_flags("ORTool_jetR10", "", or_outputLabel);
  // use "standard" setting, i.e. no need to set anything in flags
  or_flags.outputPassValue = or_outputPassValue;
  or_flags.doElectrons = isZeeJet();
  or_flags.doMuons = isZmmJet();
  or_flags.doJets = true;
  or_flags.doTaus = false;
  or_flags.doPhotons = false;
  or_flags.doFatJets = true;
  ANA_CHECK( ORUtils::recommendedTools( or_flags, or_toolbox ) );
  ANA_CHECK( or_toolbox.initialize() );

  return StatusCode::SUCCESS;
}



StatusCode EventSelection_ZjetR10 :: execute ()
{
  EventSelection::execute();
  initialize_leaves();

  xAOD::ElectronContainer* electrons = nullptr;
  if( isZeeJet() ) ANA_CHECK (evtStore()->retrieve (electrons, elec_collection));

  xAOD::MuonContainer* muons = nullptr;
  if( isZmmJet() ) ANA_CHECK (evtStore()->retrieve (muons, muon_collection));

  xAOD::JetContainer* jetsR04 = nullptr;
  ANA_CHECK (evtStore()->retrieve (jetsR04, jetR04_collection));

  xAOD::JetContainer* jetsR10 = nullptr;
  ANA_CHECK (evtStore()->retrieve (jetsR10, jetR10_collection));

  ANA_MSG_DEBUG("contrainer all retrieved");

  // overlap removal
  // MET has its own and is not affected by overlap removal
  // calling ATLAS "standard" overlap removal
  // (electrons, muons, jets, taus, photons, fatJets)
  if(electrons) ANA_MSG_DEBUG("OR_flag electron_n: " << electrons->size() );
  if(muons)     ANA_MSG_DEBUG("OR_flag muon_n: " << muons->size() );
  if(jetsR04)   ANA_MSG_DEBUG("OR_flag jetsR04_n: " << jetsR04->size() );
  if(jetsR10)   ANA_MSG_DEBUG("OR_flag jetsR10_n: " << jetsR10->size() );
  ANA_CHECK( or_toolbox.masterTool->removeOverlaps( electrons, muons, jetsR04, nullptr, nullptr, jetsR10 ) );
  // the above only decorate, now actuall remove them
  or_clean( electrons, or_outputLabel, or_outputPassValue);
  or_clean( muons, or_outputLabel, or_outputPassValue);
  or_clean( jetsR04, or_outputLabel, or_outputPassValue);
  or_clean( jetsR10, or_outputLabel, or_outputPassValue);
  ANA_MSG_DEBUG("standard OR done");
  if(electrons) ANA_MSG_DEBUG("OR_rm electron_n: " << electrons->size() );
  if(muons)     ANA_MSG_DEBUG("OR_rm muon_n: " << muons->size() );
  if(jetsR04)   ANA_MSG_DEBUG("OR_rm jetsR04_n: " << jetsR04->size() );
  if(jetsR10)   ANA_MSG_DEBUG("OR_rm jetsR10_n: " << jetsR10->size() );
  // ATTENTION large-R jet calib requires dR(j,J) > 1.4 beyond the standard "1.0"
  or_removal( jetsR10, jetsR04, 1.4);
  ANA_MSG_DEBUG("largeR jet additional OR done");
  ANA_MSG_DEBUG("OR_rm_additional jetsR04_n: " << jetsR04->size() );
  ANA_MSG_DEBUG("OR_rm_additional jetsR10_n: " << jetsR10->size() );

  // sort containers by pT
  if( isZeeJet() ) electrons->sort( descending_pt );
  if( isZmmJet() ) muons->sort( descending_pt );
  if(jetsR04)   jetsR04->sort( descending_pt );
  if(jetsR10)   jetsR10->sort( descending_pt );
  ANA_MSG_DEBUG("pt-order sorting done");

  // define physics variables
  TLorentzVector lep0, lep1, z, jet0, jet1, fjet0;

  // SF -> weight
  // TODO
  weight_mc = (isdata ? 1: eventInfo->mcEventWeight(0));
  weight *= weight_mc;

  /////////////////////
  // event selection //
  /////////////////////
  ANA_MSG_DEBUG("start event selection");

  // no cut
  pass_init = 1;
  pass_all = pass_init;
  if( pass_init ) cutflow_fill("init", 1, weight);

  // good run list
  pass_grl = 1;
  if( isdata ) pass_grl = (grl->passRunLB(*eventInfo));
  pass_all &= pass_grl;
  if( pass_all ) cutflow_fill("grl",1,weight);

  // triggers
  pass_trigger = pass_trig();
  pass_all &= pass_trigger;
  if( pass_all ) cutflow_fill("trigger", 1, weight);

  // primary vertex
  pass_primary_vertex = pass_primvertex();
  pass_all &= pass_primary_vertex;
  if( pass_all ) cutflow_fill("primary vertex", 1, weight);

  // LarError, TileError, incomplete events revoved in data
  pass_dq = pass_alldq();
  pass_all &= pass_dq;
  if( pass_all ) cutflow_fill("data quality", 1, weight);

  // event cleaning with jet
  // https://twiki.cern.ch/twiki/bin/view/AtlasProtected/HowToCleanJetsR21
  // "If you use the standard loose cleaning working point, you are advised to simply veto events for which the eventClean flag is 0."
  static SG::AuxElement::ConstAccessor<char> eventClean_LooseBadJet("DFCommonJets_eventClean_LooseBad");
  // alternatively eventInfo->auxdata< char >("DFCommonJets_eventClean_LooseBad")
  if( eventClean_LooseBadJet(*eventInfo) )  pass_jetclean = 1;
  pass_all &= pass_jetclean;
  if( pass_all ) cutflow_fill("jet clean", 1, weight);

  ANA_MSG_DEBUG("event selection: trigger dq cleaning etc. done");

  // two leptons
  if( isZeeJet() )
    pass_twolepton = (2==electrons->size());
  if( isZmmJet() )
    pass_twolepton = (2==muons->size());
  pass_all &= pass_twolepton;
  if( pass_all ) cutflow_fill("two lepton", 1, weight);

  // trigger matching
  if( pass_twolepton ){
    if( isZeeJet() ) pass_trigmatch = pass_trigmch( electrons );
    if( isZmmJet() ) pass_trigmatch = pass_trigmch( muons );
  }
  pass_all &= pass_trigmatch;
  if( pass_all ) cutflow_fill("trigger match", 1, weight);


  // opposite signs of two leptons
  if( pass_twolepton ){
    if( isZeeJet() )
      pass_opposign = ( electrons->at(0)->charge() * electrons->at(1)->charge() < 0 );
    if( isZmmJet() )
      pass_opposign = ( muons->at(0)->charge() * muons->at(1)->charge() < 0 );
  }
  pass_all &= pass_opposign;
  if( pass_all ) cutflow_fill("opposite sign", 1, weight);

  // mll window
  if( pass_twolepton ){
    double _mll = 0;
	if( isZeeJet() ) _mll = m01( *electrons );
	if( isZmmJet() ) _mll = m01( *muons );
	if( _mll > 66 && _mll < 116 ) pass_mll = 1;
  }
  pass_all &= pass_mll;
  if( pass_all ) cutflow_fill("mll", 1, weight);

  ANA_MSG_DEBUG("event selection: lepton done");

  // fjet
  pass_onefatj = ( jetsR10->size() > 0 );
  pass_all &= pass_onefatj;
  if( pass_all ) cutflow_fill("one+ fatjet", 1, weight);

  // deltaPhi(J,Z)
  if( pass_onefatj && pass_twolepton ){
	if( isZeeJet() ) z = electrons->at(0)->p4() + electrons->at(1)->p4();
	if( isZmmJet() ) z = muons->at(0)->p4() + muons->at(1)->p4();
	fjet0 = jetsR10->at(0)->p4();
	dphi_fj0_z = std::abs(fjet0.DeltaPhi(z));
	pass_dphi_fjz = ( dphi_fj0_z > 2.8 );
  }
  pass_all &= pass_dphi_fjz;
  if( pass_all ) cutflow_fill("dPhi(fjet0, Z)", 1, weight);

  // small-R jet pT to suppress radiation
  // any small-R jet pT > x, kill the event
  if( pass_twolepton ){
    pass_jpt = 1;
    double _zpt10 = z.Pt()*GeV*0.1;
	double _cut = (_zpt10 > 15) ? _zpt10 : 15;
    for( auto _j : *jetsR04 ){
      if( _j->pt()*GeV > _cut ) pass_jpt = 0;
    }
  }
  pass_all &= pass_jpt;
  if( pass_all ) cutflow_fill("veto r04 jet pT",1,weight);

  ANA_MSG_DEBUG("event selection: largeR jet done");

  /////////////////////
  //  tree filling   //
  /////////////////////

  //
  runnumber = eventInfo->runNumber ();
  eventnumber = eventInfo->eventNumber ();

  //
  if( isZeeJet() ){
    if( electrons->size()>0 ){
	  lep0 = TLorentzVector( electrons->at(0)->p4() );
      lep0_pt = lep0.Pt()*GeV;
	  lep0_eta = lep0.Eta();
	  lep0_phi = lep0.Phi();
	  lep0_e = lep0.E()*GeV;
	}
	if( electrons->size()>1 ){
	  lep1 = TLorentzVector( electrons->at(1)->p4() );
      lep1_pt = lep1.Pt()*GeV;
	  lep1_eta = lep1.Eta();
	  lep1_phi = lep1.Phi();
	  lep1_e = lep1.E()*GeV;
	}
  }

  if( isZmmJet() ){
    if( muons->size()>0 ){
	  lep0 = TLorentzVector( muons->at(0)->p4() );
      lep0_pt = lep0.Pt()*GeV;
	  lep0_eta = lep0.Eta();
	  lep0_phi = lep0.Phi();
	  lep0_e = lep0.E()*GeV;
	}
	if( muons->size()>1 ){
	  lep1 = TLorentzVector( muons->at(1)->p4() );
      lep1_pt = lep1.Pt()*GeV;
	  lep1_eta = lep1.Eta();
	  lep1_phi = lep1.Phi();
	  lep1_e = lep1.E()*GeV;
	}
  }

  jet_n = jetsR04->size();

  if( jetsR04->size()>0 ){
    jet0 = TLorentzVector( jetsR04->at(0)->p4() );
    jet0_pt = jet0.Pt()*GeV;
	jet0_eta = jet0.Eta();
	jet0_phi = jet0.Phi();
	jet0_e = jet0.E()*GeV;
  }
  if( jetsR04->size()>1 ){
    jet1 = TLorentzVector( jetsR04->at(1)->p4() );
    jet1_pt = jet1.Pt()*GeV;
	jet1_eta = jet1.Eta();
	jet1_phi = jet1.Phi();
	jet1_e = jet1.E()*GeV;
  }

  fjet_n = jetsR10->size();

  if( jetsR10->size()>0 ){
    fjet0 = TLorentzVector( jetsR10->at(0)->p4() );
    fjet0_pt = fjet0.Pt()*GeV;
	fjet0_eta = fjet0.Eta();
	fjet0_phi = fjet0.Phi();
	fjet0_e = fjet0.E()*GeV;
  }

  // // //

  if( pass_twolepton ){
    z_pt = z.Pt()*GeV;
	z_eta = z.Eta();
	z_phi = z.Phi();
	z_e = z.E()*GeV;
	z_m = z.M()*GeV;
  }

  // met TODO


  if( pass_all ){

	TVector3 _ref_2d( z.Px()*GeV, z.Py()*GeV, 0 );

	// DB
	TVector3 _j_2d( fjet0.Px()*GeV, fjet0.Py()*GeV, 0 );
    resp_db = fjet0.Pt()*GeV / ( std::abs(_j_2d.Dot(_ref_2d) / _j_2d.Mag()) );

	// MPF
	// R10 no MET no MPF
  }

  ANA_MSG_DEBUG("fill tree start");
  tree ("vjet")->Fill ();
  ANA_MSG_DEBUG("fill tree all done");

  return StatusCode::SUCCESS;
}

StatusCode EventSelection_ZjetR10 :: finalize ()
{
  EventSelection::finalize();

  return StatusCode::SUCCESS;
}

} // namespace VjTM
