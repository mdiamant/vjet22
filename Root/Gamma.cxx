#include <AsgMessaging/MessageCheck.h>
#include <VjetTreeMaker/Gamma.h>

namespace VjTM{

Gamma :: Gamma (const std::string& name,
                                  ISvcLocator *pSvcLocator)
    : ObjectSelection (name, pSvcLocator),
	  calibration("EgammaCalibrationAndSmearingTool/calibration", this),
	  selector("AsgPhotonIsEMSelector/selector", this),
	  isolation("IsolationSelectionTool/isolation", this)
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  This is also where you
  // declare all properties for your algorithm.  Note that things like
  // resetting statistics variables or booking histograms should
  // rather go into the initialize() function.

  declareProperty("calibration", calibration, "egamma calibration tool");
  declareProperty("selector", selector, "egamma ID tool");
  declareProperty("isolation", isolation, "egamma isolation tool");

}



StatusCode Gamma :: initialize ()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.

  ANA_CHECK ( calibration.retrieve() );

  return StatusCode::SUCCESS;
}



StatusCode Gamma :: execute ()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.

  const xAOD::PhotonContainer* in_gammas = nullptr;
  ANA_CHECK (evtStore()->retrieve (in_gammas, in_collection));

  auto out_gammas = std::make_unique<xAOD::PhotonContainer>();
  auto out_gammas_aux = std::make_unique<xAOD::AuxContainerBase>();
  out_gammas->setStore( out_gammas_aux.get() );

  for ( auto *igamma : *in_gammas ) {
    xAOD::Photon * gamma = nullptr;

	// calibration
    calibration->correctedCopy(*igamma, gamma);

	// selection
	bool is_pT = false;
	bool is_eta = false;
	bool is_selector = false;
	bool is_iso = false;

	is_pT = ( gamma->pt()*GeV > pTmin );
	is_eta = ( std::abs(gamma->eta()) < etamax );
   // is_selector = ( selector->accept( gamma ) ); //HERE
	//is_iso = ( isolation->accept( *gamma ) ); //HERE

    if( is_pT && is_eta && selector->accept( gamma ) &&  isolation->accept( *gamma )){
	  auto gamma_sel = new xAOD::Photon();
	  out_gammas->push_back( gamma_sel );
	  *gamma_sel = *gamma;
	  xAOD::setOriginalObjectLink( *igamma, *gamma_sel ); // to help metMap to find the originals
      // just in case this was not done in calibration()
	}

	delete gamma;
  }

  ANA_CHECK (evtStore()->record (out_gammas.release(), out_collection));
  ANA_CHECK (evtStore()->record (out_gammas_aux.release(), Form("%sAux.", out_collection.c_str()) )); // "Aux." !!!

  return StatusCode::SUCCESS;
}



StatusCode Gamma :: finalize ()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.

  // must have it to store cutflow
  ObjectSelection :: finalize();

  return StatusCode::SUCCESS;
}

} // namespace VjTM
