#include <AsgMessaging/MessageCheck.h>
#include <VjetTreeMaker/Tree_ZjetR10.h>

namespace VjTM{

void Tree_ZjetR10 :: initialize_tree( TTree* tr ){

  // connect to the external tree
  thetree = tr;

  // connect to branches

  // event basic info
  thetree->Branch("runnumber", &runnumber);
  thetree->Branch("eventnumber", &eventnumber);

  // weights
  thetree->Branch("weight", &weight);
  //double weight_ electron muon SF etc.
  thetree->Branch("weight_mc", &weight_mc);
  thetree->Branch("weight_pileup", &weight_pileup);

  // flag of pass event selections
  thetree->Branch("pass_all", &pass_all);
  thetree->Branch("pass_init", &pass_init);
  thetree->Branch("pass_grl", &pass_grl);
  thetree->Branch("pass_trigger", &pass_trigger);
  thetree->Branch("pass_trigmatch", &pass_trigmatch);
  thetree->Branch("pass_primary_vertex", &pass_primary_vertex);
  thetree->Branch("pass_dq", &pass_dq);
  thetree->Branch("pass_jetclean", &pass_jetclean);
  thetree->Branch("pass_twolepton", &pass_twolepton);
  thetree->Branch("pass_opposign", &pass_opposign);
  thetree->Branch("pass_mll", &pass_mll);
  thetree->Branch("pass_onefatj", &pass_onefatj);
  thetree->Branch("pass_dphi_fjz", &pass_dphi_fjz);
  thetree->Branch("pass_jpt", &pass_jpt);

  // variables
  thetree->Branch("lep0_pt", &lep0_pt);
  thetree->Branch("lep0_eta", &lep0_eta);
  thetree->Branch("lep0_phi", &lep0_phi);
  thetree->Branch("lep0_e", &lep0_e);
  
  thetree->Branch("lep1_pt", &lep1_pt);
  thetree->Branch("lep1_eta", &lep1_eta);
  thetree->Branch("lep1_phi", &lep1_phi);
  thetree->Branch("lep1_e", &lep1_e);
 
  thetree->Branch("z_pt", &z_pt);
  thetree->Branch("z_eta", &z_eta);
  thetree->Branch("z_phi", &z_phi);
  thetree->Branch("z_e", &z_e);
  thetree->Branch("z_m", &z_m);

  thetree->Branch("jet_n", &jet_n);

  thetree->Branch("jet0_pt", &jet0_pt);
  thetree->Branch("jet0_eta", &jet0_eta);
  thetree->Branch("jet0_phi", &jet0_phi);
  thetree->Branch("jet0_e", &jet0_e);

  thetree->Branch("jet1_pt", &jet1_pt);
  thetree->Branch("jet1_eta", &jet1_eta);
  thetree->Branch("jet1_phi", &jet1_phi);
  thetree->Branch("jet1_e", &jet1_e);

  thetree->Branch("fjet_n", &fjet_n);

  thetree->Branch("fjet0_pt", &fjet0_pt);
  thetree->Branch("fjet0_eta", &fjet0_eta);
  thetree->Branch("fjet0_phi", &fjet0_phi);
  thetree->Branch("fjet0_e", &fjet0_e);

  thetree->Branch("dphi_fj0_z", &dphi_fj0_z);

  thetree->Branch("met_x", &met_x);
  thetree->Branch("met_y", &met_y);
  thetree->Branch("met_phi", &met_phi);
  thetree->Branch("met", &met);

  thetree->Branch("resp_db", &resp_db);
  thetree->Branch("resp_mpf", &resp_mpf);

}

void Tree_ZjetR10 :: initialize_leaves(){

  // this should be called before filling any value to variables
  // such as the begining of execute() of the event selection

  // event basic info
  runnumber = 0; ///< Run number
  eventnumber = 0; ///< Event number

  // weights
  weight = 1;
  //double weight_ electron muon SF etc.
  weight_mc = 1; // matrix element
  weight_pileup = 1;
  
  // flag of pass event selections
  pass_all = 0;
  pass_init = 0;
  pass_grl = 0;
  pass_trigger = 0;
  pass_trigmatch = 0;
  pass_primary_vertex = 0;
  pass_dq = 0;
  pass_jetclean = 0;
  pass_twolepton = 0;
  pass_opposign = 0;
  pass_mll = 0;
  pass_onefatj = 0;
  pass_dphi_fjz = 0;
  pass_jpt = 0;

  // variables
  lep0_pt = 0;
  lep0_eta = 0;
  lep0_phi = 0;
  lep0_e = 0;

  lep1_pt = 0;
  lep1_eta = 0;
  lep1_phi = 0;
  lep1_e = 0;

  z_pt = 0;
  z_eta = 0;
  z_phi = 0;
  z_e = 0;
  z_m = 0;

  jet_n = 0;

  jet0_pt = 0;
  jet0_eta = 0;
  jet0_phi = 0;
  jet0_e = 0;

  jet1_pt = 0;
  jet1_eta = 0;
  jet1_phi = 0;
  jet1_e = 0;

  fjet_n = 0;

  fjet0_pt = 0;
  fjet0_eta = 0;
  fjet0_phi = 0;
  fjet0_e = 0;

  dphi_fj0_z = 0;

  met_x = 0;
  met_y = 0;
  met_phi = 0;
  met = 0;

  resp_db = 0;
  resp_mpf = 0;
}

} // namespace VjTM
